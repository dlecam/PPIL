<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemandeInscription extends Model {


    protected $table = 'demandeInscriptions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['statutDemande', 'utilisateur_id'];
    

    /**
     * recupère l'utilisateur qui fait la demande d'inscription.
     */
    public function utilisateur() {
        return $this->belongsTo('App\Utilisateur');
    }

}