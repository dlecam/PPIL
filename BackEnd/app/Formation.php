<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formation extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     	'formation', 'libelle', 'annee'
    ];

    
    /**
     * recupère les responsables gérant une formation.
     */
    public function utilisateurs() {
    	return $this->belongsToMany('App\Utilisateur');
    }


    /**
     * recupère les UE(s) appartenant à une formation.
     */
    public function ues() {
        return $this->hasMany('App\Ue');
    }

}