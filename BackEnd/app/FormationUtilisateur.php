<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormationUtilisateur extends Model {
	protected $table = 'formation_utilisateur';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     	'formation_id', 'utilisateur_id'
    ];

    
    /**
     * recupère les responsables gérant une formation.
     */
    public function utilisateur() {
    	return $this->belongsTo('App\Utilisateur');
    }


    /**
     * recupère les UE(s) appartenant à une formation.
     */
    public function formation() {
        return $this->belongsTo('App\Formation');
    }

}