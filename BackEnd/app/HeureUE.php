<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeureUE extends Model {
    protected $table = 'heuresUE';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ue_id', 'utilisateur_id', 'volume', 'type', 'valide'
	];
    


    /**
     * recupère l'utilisateur qui fait la demande d'intervention pour une UE.
     */
    public function utilisateur() {
        return $this->belongsTo('App\Utilisateur');
    }


    /**
     * recupère l'UE concernée par cette demande d'intervention.
     */
    public function ue() {
        return $this->belongsTo('App\Ue');
    }

}