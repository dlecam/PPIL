<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ConnexionController extends Controller {
	public function deconnexion(Request $request) {
		if (session_status() == PHP_SESSION_ACTIVE) {
			unset($_SESSION['email']);
			session_destroy();
		}
		setcookie('email', null, -1, "/", null, false, true);
		setcookie('password', null, -1, "/", null, false, true);
		return redirect()->route('connexion');
    }
}


