<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\DemandeInscription;
use Validator ;
use app\Repositories\MailRepository;

use App\Utilisateur as Utilisateur;
use App\FormationUtilisateur as FormationUtilisateur;
use App\Ue as Ue;
use App\HeureUE as Heure;


class ConsultModifUEformationControllers extends Controller {

	public function showPage() {
		$results = array();
                
		$user = Utilisateur::where('email', $_SESSION['email'])->first();
		$user_id = $user->id;
                
		$formations = FormationUtilisateur::where('utilisateur_id',$user_id)->get();
		
		$listeUe = array();
		$countForm = 1;
		foreach ($formations as $formation) {
			
			$formationObj = $formation->formation()->first();
			
			$form = array();
			$form['nom'] = $formationObj->libelle;
			$form['annee'] = $formationObj->annee;
			$form['formation'] = $formationObj->formation;
			
			$ues = Ue::where('formation_id', $formationObj->id)->get();
			
			$countUE = 1;
			
			foreach ($ues as $ue) {
				$ueElem = array();
				$ueElem['nom'] = $ue->libelle;
				$ueElem['groupeTP'] = $ue->nombreGroupeTP;
				$ueElem['groupeTD'] = $ue->nombreGroupeTD;
				$ueElem['groupeEI'] = $ue->nombreGroupeEI;
				
				$heureTotales = 0;
				$heureTD = 0;
				$heureTP = 0;
				$heureEI = 0;
				$heureCM = 0;
				
				$listeHeure = Heure::where([['ue_id', $ue->id], ['valide', true]])->get();
				foreach ($listeHeure as $elemHeure) {
					switch ($elemHeure->type) {
						case "CM": $heureTotales += $elemHeure->volume * 3/2; $heureCM += $elemHeure->volume; break;
						case "TD": $heureTotales += ($elemHeure->volume)/$ue->nombreGroupeTD; $heureTD += $elemHeure->volume; break;
						case "TP": $heureTotales += ($elemHeure->volume * (2/3))/$ue->nombreGroupeTP; $heureTP += $elemHeure->volume; break;
						case "EI": $heureTotales += ($elemHeure->volume * (7/6))/$ue->nombreGroupeEI; $heureEI += $elemHeure->volume; break;
					}
				}
				
				$ueElem['Total'] = $heureTotales;
				$ueElem['CM'] = $heureCM;
				$ueElem['TP'] = $heureTP;
				$ueElem['EI'] = $heureEI;
				$ueElem['TD'] = $heureTD;
					
				$listeUe[$countUE] = $ueElem;
				$countUE += 1;
			}
			
			$form['ues'] = $listeUe;
			
			$results[$countForm] = $form;
			$countForm += 1;
		}
		
		return view('pageUEFormation', ['formations' => $results]);
	}
	
	

	public function listeUes(){
		$ue=DB::table('ues')->get();
		return View('pageUEFormation')->with('listeues',$ue)->with('errors',"amal");
	}

	public function modifier(Request $request){
		$ue=DB::table('ues')->get();

		$nombreheures=DB::table('HeuresUE')->where('ue_id',$request->input('id'))->where('valide',1)->sum('volume');

		$validator = Validator::make($request->all(), [
           'libelle' => 'required|min:2|max:30|string',
           'volumeUE' => 'required|numeric|min:'.$nombreheures.'',
           'nombreGroupeTD' => 'required|numeric',
           'nombreGroupeTP' => 'required|numeric',
           'nombreGroupeEI' => 'required|numeric',
        ],[
           'required'=>'ce champs est vide',
           'min'=>'ce champs doit contenir au minimum 2 characteres',
           'string'=>'ce champs ne doit contenir que des lettres',
           'numeric'=>'ce champs doit etre un nombre', 
        ]
        );

        if ($validator->fails()) { 

            return View('pageUEFormation')->with('listeues',$ue)->with('errors',$validator->errors());
        }

		$ue=DB::table('ues')->where('id',$request->input('id'))
			->update([
				'libelle'=>$request->input('libelle'),
				'volumeUE'=>$request->input('volumeUE'),
				'nombreGroupeEI'=>$request->input('nombreGroupeEI'),
				'nombreGroupeTD'=>$request->input('nombreGroupeTD'),
				'nombreGroupeTP'=>$request->input('nombreGroupeTP'),
				'utilisateur_id'=>$request->input('utilisateur_id'),
				]);
    }
}
