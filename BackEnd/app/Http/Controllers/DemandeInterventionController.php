<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;

use App\Utilisateur as Utilisateur;
use App\UeUtilisateur as Association;
use App\Ue as Ue;

class DemandeInterventionController extends Controller {

    
	public function incriptionUE(Request $request) {

		$ue = $request->input("ue");
		$user = Utilisateur::where('email', $_SESSION['email'])->first();
		
		if (Ue::where('id', $ue)->count() > 0) {
			if (Association::where([['ue_id', $ue], ['utilisateur_id', $user->id]])->count() == 0) {
				Association::create(['ue_id' => $ue, 'utilisateur_id' => $user->id, 'valide' => false]);
			}
		}

		return redirect()->route('mesCours');
	}
	
	
	
	
	
    
    public function ajouterUE(Request $request){
        $email = $_COOKIE['email'];
        $user = DB::table('utilisaturs')->where('email', $email)->first();
        //recupération de l'id de l'utilisateur
        $id=$user->id;
        $id_ue = $request->input('id_ue');
        $heure_cm= $request->input('heure_cm');
        $heure_tp = $request->input('heure_tp');
        $heure_td = $request->input('heure_td');
        $heure_ei = $request->input('heure_ei');
        $nb_groupes_tp = $request->input('nb_groupes_tp');
        $nb_groupes_td = $request->input('nb_groupes_td');
        $nb_groupes_ei = $request->input('nb_groupes_ei');
        DB::table('heure_UE')->insert(
               ['ue_id' => $id_ue , 'utilisateur_id' => $id ,'volume' => $heure_cm , 'type' => 'CM' , 'valide' => false ]
        );
        DB::table('heure_UE')->insert(
               ['ue_id' => $id_ue , 'utilisateur_id' => $id ,'volume' => $heure_tp , 'type' => 'TP' , 'valide' => false ]
        );
        DB::table('heure_UE')->insert(
               ['ue_id' => $id_ue , 'utilisateur_id' => $id ,'volume' => $heure_td , 'type' => 'TD' , 'valide' => false ]
        );
        DB::table('heure_UE')->insert(
               ['ue_id' => $id_ue , 'utilisateur_id' => $id ,'volume' => $heure_ei , 'type' => 'EI' , 'valide' => false ]
        );
        /*
        la page des enseignements de l'enseignant 

        return redirect('mesEnseignements')->with('message', 'le demande d\'intrevention est enregistré ' );
        */

        
    }
}