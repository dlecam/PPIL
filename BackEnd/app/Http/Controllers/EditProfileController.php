<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Utilisateur;
use App\Statut;
use Validator ;

class EditProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // empty
    }
    
    public function showInfo(){
        
      /*  if (isset($_COOKIE['email'])) {
            $email = $_COOKIE['email'];
        } else {
            $email=null; // to fix after
        }   */
        
        
        $user = Utilisateur::where('email',$_COOKIE['email'])->first();
        
        $status = Statut::where('id',$user->id)->first();
        
        
        
      //  $array = json_decode(json_encode($user), True);
        $array = $user->toArray();
        $array['status'] = $status['libelle'];
        
        
        return view('info_profil', ['user' => $array]);
        
    }
    
    
    public function modify(Request $request){
        
        // modification des input
          $input = array_map('trim', $request->all());
          $input['nom'] = $input['newNom'];
          unset($input['newNom']);
          $input['prenom'] = $input['newPrenom'];
          unset($input['newPrenom']);

          $input['email'] = $input['newEmail'];
          unset($input['newEmail']);


          $request->replace($input);
        
                
    //    print_r($request->all());
    //    exit();
        
        // validation des donnes
        if($request->email!=$_COOKIE['email']){
            $validator = Validator::make($request->all(), [
           'email' => 'required|unique:utilisateurs,email,'.$request->email,
           'nom' => 'required|min:4|max:30|string',
           'prenom' => 'required|min:4|max:30|string',]
           ,['email' =>'le format de l email est incorrect',
           'unique' =>'email deja pris',
           'required'=>'ce champs est vide',
           'min'=>'ce champs doit contenir au minimum 4 lettres',
           'string'=>'ce champs ne doit contenir que des lettres',
            ]);
        }else {
            $validator = Validator::make($request->all(), [
           'email' => 'required|email',
           'nom' => 'required|min:4|max:30|string',
           'prenom' => 'required|min:4|max:30|string',]
           ,['email' =>'le format de l email est incorrect',
           'unique' =>'email deja pris',
           'required'=>'ce champs est vide',
           'min'=>'ce champs doit contenir au minimum 4 lettres',
           'string'=>'ce champs ne doit contenir que des lettres',
            ]);
        }
        
       
        
        
        if ($validator->fails()) {

            return View('info_profil')->with('errors',$validator->errors());
        }
        
        
        $old_email = $_COOKIE['email'];
        $n_email = $request->input('email');
        $n_nom = $request->input('nom');
        $n_prenom = $request->input('prenom');
        
        // database update
        Utilisateur::where('email',$old_email)->update( ['nom' => $n_nom ,
                                                     'prenom' => $n_prenom , 
                                                     'email' => $n_email] );
        
        
        // new authentification
        setcookie("email", $n_email);
        $_SESSION['email'] = $n_email;
        
        // recuperation de l'utilisateur et status
        $user = Utilisateur::where('email',$n_email)->first();
        $status = Statut::where('id',$user->id)->first();
        $array = $user->toArray();
        $array['status'] = $status['libelle'];

        return view('info_profil', ['user' => $array]);
    }
}