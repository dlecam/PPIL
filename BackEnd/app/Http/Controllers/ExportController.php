<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Utilisateur;


class ExportController extends Controller {
    

    public function listeUtilisateur($fichier, $user) {
        fputs($fichier, "\"Enseignant\",");
        fputs($fichier, "\"$user->prenom\",");
        fputs($fichier, "\"$user->nom\",");
        fputs($fichier, "\"$user->email\",");
        fputs($fichier, "\"$user->estRDI\",");
        fputs($fichier, "\"$user->statut_id\"");
        fputs($fichier, "\n");
    }

    public function listeUtilisateurParId($fichier, $utilisateurId) {
        $users = DB::table('utilisateurs')->where('id', $utilisateurId)->get();
        foreach ($users as $user) {
            $this->listeUtilisateur($fichier, $user);
        }
    }

    public function listeUE($fichier, $ueId) {
        $ueUtilisateur = DB::table('ue_utilisateur')->where('ue_id', $ueId)->get();
        foreach ($ueUtilisateur as $utilisateur) {
            $this->listeUtilisateurParId($fichier, $utilisateur->utilisateur_id);
        }
    }

    public function listePourRF($fichier, $id) {
        $formations = DB::table('formation_utilisateur')->where('utilisateur_id', $id)->get();
        if ($formations->isEmpty()) {
            return false;
        }

        foreach ($formations as $formation) {
            $formationId = $formation->id;
            $ues = DB::table('ues')->where('formation_id', $formationId)->get();
            foreach ($ues as $ue) {
                $this->listeUE($fichier, $ue->id);
            }
        }

        return true;
    }

    public function listePourRUE($fichier, $id) {
        $ues = DB::table('ues')->where('utilisateur_id', $id)->get();
        if ($ues->isEmpty()) {
            return false;
        }

        foreach ($ues as $ue) {
            $this->listeUE($fichier, $ue->id);
        }

        return true;
    }

    public function listePourRDI($fichier) {
        $users = DB::table('utilisateurs')->get();
        foreach ($users as $user) {
            $this->listeUtilisateur($fichier, $user);
        }
    }

    public function export() {
        $chemin = '../tmp/';

        do {
            $nom = str_random(10) . '.csv';
        } while (file_exists($chemin . $nom));

        $chemin = $chemin . $nom;

        /* Ouverture du fichier */
        $fichier = fopen($chemin, 'a');
        if (!$fichier) {
            return redirect()->route('profil');
        }

        /* Récupération des informations de l'utilisateur */
        $infosUtilisateur = Utilisateur::where('email', $_SESSION['email'])->first();
        $estRDI = $infosUtilisateur->estRDI;
        $id = $infosUtilisateur->id;

        /* Ecriture dans le fichier, la liste
           que l'on obtient dépend des droits de l'utilisateur */
        if ($estRDI) {
            $this->listePourRDI($fichier);
        }
        else {
            $estRF = $this->listePourRF($fichier, $id);
            if (!$estRF) {
              $estRUE = $this->listePourRUE($fichier, $id);
              if (!$estRUE) {
                  $this->listeUtilisateur($fichier, $infosUtilisateur);
              }
            }
        } 

        /* Fermeture du fichier */
        if (!fclose($fichier)) {
            return redirect()->route('profil');
        }

        return response()->download($chemin);
    }

}
