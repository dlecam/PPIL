<?php

/*

Import.php, 
Responsable : Thomas Lemaire

*/

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Utilisateur as Utilisateur;
use App\Statut as Statut;
use App\Repositories\MailRepository;
use Illuminate\Support\Facades\Crypt as Crypt;

class ImportController extends Controller {
	
	private function genMDP() { // Fonction de génération d'un mdp alphanum sur 10 char.
		$char = 'aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz00112233445566778899'; 
		$mdp = str_shuffle($char); 
		$mdp = substr($mdp, 4, 10);
		return $mdp;
	}
	
	public function import(Request $request, MailRepository $mailer) {
		
		$error = true;
				
		if ($request->hasFile('fichier') && $request->file('fichier')->isValid()) {
						
			$file = $request->file('fichier');
			$continue = true;
			
			// Vérifier le type
			
			if ($file->getClientOriginalExtension() != "csv") {
				$continue = false;
			}

			// vérifier la cohérence du contenu, analyse et ajout
			
			$lines;
			
			if ($continue == true) {
				// Convertion en string 
				$content = file_get_contents($file);
				// Séparation des lignes
				
				$lines = explode(PHP_EOL, $content);
				// Pour chaque ligne
				foreach ($lines as $line) {
					
					// Séparation des éléments
					$elements = explode(';', $line);
					$valide = true;
					// Pour chaque élément
					foreach ($elements as $element) {
						// Test de la conformité à la regex
						if (preg_match('/^".*"$/', $element) == 0) {
							$valide = false;
						}
					}
					
					if ($valide) {
						
						/*
						Cn, où n = 
						1 -> utilisateur 
						2 -> prenom
						3 -> nom
						4 -> mail
						5 -> RDI
						6 -> Statut
						*/
						
						$C1 = null;
						$C2 = null;
						$C3 = null;
						$C4 = null;
						$C5 = null;
						$C6 = null;
						
						$coherent = false;
						$error = false;
						
						foreach ($elements as $element) {
							
							$value = str_replace('"', '', $element);
							
							if ($C1 == null) {
								if ($value == "Utilisateur") {
									$C1 = true;
								}
								else {
									$error = true;
								}
							}
							else if ($C2 == null) {
								$C2 = $value;
							}
							else if ($C3 == null) {
								$C3 = $value;
							}
							else if ($C4 == null) {
								$C4 = $value;
							}
							else if ($C5 == null) {
								if ($value == "true" || $value == "false") {
									$C5 = false;
									if ($value == "true") {
										$C5 = true;
									}
								}
								else {
									$error = true;
								}
							}
							else if ($C6 == null) {
								$entier = intval($value);
								$compteur = Statut::Where('id', $entier)->count();
								
								if ($compteur > 0) {
									$C6 = $entier;
									$coherent = true;
								}
							}
							
							if ($error) {
								break;
								$coherent = false;
							}

						}
												
						if ($coherent) {
							if (Utilisateur::where('email', $C4)->count() == 0) {
								$new = new Utilisateur();
								$new->prenom = $C2;
								$new->nom = $C3;
								$new->email = $C4;
								$new->estRDI = $C5;
								$new->statut_id = $C6;
								$new->inscriptionValide = true;

								$mdp = $this->genMDP();
								$new->motDePasse = Crypt::encrypt($mdp);

								$new->save();	

								$sujet = "Ajout de votre profil à l'application. No Reply !";
								$message_txt = "Bonjour\nVous venez d'être ajouté à l'application.\nVotre mot de passe est $mdp";
								$message_html = "<html><head></head><body><b>Bonjour</b><br/>mail envoye automatiquement, ne pas repondre. <br/>Vous venez d'être ajouté à l'application <br>Votre mot de passe est $mdp .<br/>Vous pouvez changer ce mot de passe a tout moment sur votre page de profil.</body></html>";
								//fin contenu du mail.
								$mailer->sendMail($C4, $message_txt, $message_html, $sujet);
							}
						}
						
						unset($C1);
						unset($C2);
						unset($C3);
						unset($C4);
						unset($C5);
						unset($C6);
					}
				}
			}
		}
	}
}
