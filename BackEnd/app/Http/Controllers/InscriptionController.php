<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Utilisateur;
use App\DemandeInscription;
use Validator ;
use App\Repositories\MailRepository;
use Illuminate\Support\Facades\Crypt;

class InscriptionController extends Controller {


	public function inscrire(Request $request){


		
		$validator = Validator::make($request->all(), [
           'email' => 'required|unique:utilisateurs|email',
           'nom' => 'required|min:4|max:30|alpha',
           'prenom' => 'required|min:4|max:30|alpha',
   		   'password'=>'required|min:4',
   		   //'confpassword'=>'same:password',
   		   'statut'=>'not_in:0',

        ],['email' =>'le format de l email est incorrect',
           'unique' =>'email deja pris',
           'required'=>'ce champs est vide',
           'min'=>'ce champs doit contenir au minimum 4 lettres',
           'string'=>'ce champs ne doit contenir que des lettres',
           'same'=>'les mots de passe pas similaires',
           'not_in'=>'veuillez choisir un statut',
        ]
        );

		$user=Utilisateur::where('email',$request->input('email'))->get();
        if ($validator->fails()) { 

            return View('inscription')->with('errors',$validator->errors());
        }

		$utilisateur=Utilisateur::create([
			'nom' =>$request->input('nom'),
			'prenom' =>$request->input('prenom'),
			'email'=> $request->input('email'),
			'motDePasse' => Crypt::encrypt($request->input('password')),
			'photo'=>'',
			'estRDI'=>0,
			'inscriptionValide'=>0,
			'statut_id'=>$request->input('statut'),
		]);
		$demandeInscription=DemandeInscription::create([
			'statutDemande' =>'EN ATTENTE',
			'utilisateur_id'=>$utilisateur->id,
		]);
		//sendMail($mail, $msgTxt, $msgHtml, $subject);
		$var=new MailRepository();
		$var->sendMail($request->input('email'), "votre a demande a ete enregistree et sera traite","<html>votre a demande a ete enregistree et sera traite</html>", "demande inscription");

		return redirect()->route('connexion');	
    }
}
