<?php
 
namespace App\Http\Controllers;
 
use App\Utilisateur;
use App\Ue;
use App\UeUtilisateur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
class InscriptionUEController extends Controller{
 
 
    public function __construct() {

    }


    // Retourne la page contenant toutes les demandes inscription de l'utilisateur

    public function listeInscriptionsUE() {

/*        // on récupère l'id de l'utilisateur qui est connecté
        $user = Utilisateur::where('email', $_SESSION['email'])->first();

        // on récupère la liste de toutes ses demandes interventions qui sont validées ou non !
        $inscriptionsUE = UeUtilisateur::where('utilisateur_id' , $user->id)->get();
        
        // ou bien  $inscriptionsUE = $user->ueutilisateurs();

        // on le redirige vers la page qui affiche la liste des demandes et dans notre cas ici, ce sera la page mesCours en lui injectant la liste des inscriptions!
        
        //return View("inscription_ue", compact('inscriptionsUE'));*/
        
        $email = $_SESSION['email'];
        $user = DB::table('utilisateurs')->where('email',$email)->first();
        $uesInscrits = DB::table('ue_utilisateur')->where('utilisateur_id',$user->id)->get(); 
        $ues = DB::table('ues')->get();
        $bool = FALSE;
        $tableau = array();
        $cpt = 1;
        foreach ($ues as $ue) {
            $bool=TRUE;
            foreach($uesInscrits as $ueInscrit) {
                if ($ue->id == $ueInscrit->ue_id) {
                    $bool = FALSE;
                } 
            }
            if($bool) {
                $tableau[$cpt] = array();
                $tmp = array();
                $formation = DB::table('formations')->where('id',$ue->formation_id)->first();
                $tmp['idUE'] = $ue->id;              
                $tmp['nomFormation'] = $formation->formation.$formation->annee;
                $tmp['nomUE'] = $ue->libelle;              
                $tableau[$cpt] = $tmp;
                $cpt += 1;                
            }
        }

        return view('inscription_ue',['formations' => $tableau]);
 
        
        /*  formation
         *          UE
         */
    }
 

    // creer une demande inscription ue pour l'utilisateur connecté 
    public function creerInscriptionUE(Request $request) {

        // on récupère le tableau des ues cochées dans le formulaire
        $ues = Input::get('ues');

        // règles de validation pour savoir si des données ont été postées et si l'UE cochée existe 
        $validator = Validator::make($ues, 
            [
                "ues" => "array|required",
                "ues.*" => "numeric|exists:ue_id, id",
                
            ],
            [
                "ues" => "le format des données postées est incorrect !",
                "ues.*" => "cette UE n'existe pas !"
            ]
        );
        

        // on récupère l'id de l'utilisateur qui est connecté
        $user = Utilisateur::where('email', $_SESSION['email'])->first();

        // si la soumission formulaire non valide alors rediriger vers la page courante pour afficher les erreurs
        if ($validator->fails()) {
            //return View('inscription_ue')->with('errors',$validator->errors());
            echo "Erreurs lors de la soumission du formulaire";
        } else 
        {   
            // on récupère les UE dans lesquelles l'utilisateur est inscrit grâce à l'association
            $ues_deja_inscrites = $user->ueutilisateurs()->pluck('ue_id');
            
            foreach ($ues as $ue) {
                // si l'utilisateur n'est pas encore inscrit dans l'UE 
                if(!in_array($ue, $ues_deja_inscrites, false)) {
                    $inscriptionUE = UeUtilisateur::create([
                        'ue_id' => (int) $ue,
                        'utilisateur_id' => $user->id,
                        'valide' => null,         
                    ]);
                    echo "Votre inscription à l'UE " . $ue ." a bien été prise en compte";
                    echo "</br>";
                } else {
                    echo "Vous êtes déjà inscrit à l'UE " . $ue;
                    echo "</br>";
                }
            }
        }
    }


    // teste si l'utilisateur est déjà inscrit dans l'UE (id de l'UE passé en paramètre)

    private function dejaInscritUE(array $ues, $id) {
       // pas besoin
    }

    
    public function accepterInscriptionUE($id) {

         // on récupère l'id de l'utilisateur qui est connecté
        $user = Utilisateur::where('email', $_SESSION['email'])->first();

        // on valide l'ue placé en paramètre
        $ue = UeUtilisateur::where('utilisateur_id' , $user->id)->where('ue_id' , $id)->update(['valide' => 1]);

        return redirect()->route('profil');
    }


    public function refuserInscriptionUE($id) {

         // on récupère l'id de l'utilisateur qui est connecté
        $user = Utilisateur::where('email', $_SESSION['email'])->first();

        // on valide l'ue placé en paramètre
        $ue = UeUtilisateur::where('utilisateur_id' , $user->id)->where('ue_id' , $id)->update(['valide' => 0]);

        return redirect()->route('profil');  

    }
 
}
 