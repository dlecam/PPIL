<?php

/*

IntervenantController.php, 
Modifié mardi 23 mai 2017,
Responsable : Thomas Lemaire

*/

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\UeUtilisateur as Association;
use App\Utilisateur as Utilisateur;
use App\Ue as Ue;
use App\Formation as Formation;
use App\HeureUE as Heure;

class IntervenantController extends Controller {

	private function droitsConformesModification($request) {

		$user = Utilisateur::where('email', $_SESSION['email'])->first();

		if ($user == null) {
			return false;
		}

		// Vérification qu'il est responsable de cette UE

		$ue = $request->input("ue");

		if ($ue != null) {
			$UE = Ue::where('id', $ue)->first();
			if ($UE != null && $UE->utilisateur_id == $user->id) {
				return true;
			}
		}

		return false;
	}
	
	private function droitsConformesVision() {
		
		$user = Utilisateur::where('email', $_SESSION['email'])->first();

		if ($user == null) {
			return false;
		}

		// Vérification qu'il est responsable d'au moins une UE

		$count = Ue::where('utilisateur_id', $user->id)->count();
		if ($count > 0) {
			return true;
		}

		return false;
	}

	private function infoProf($id, $ue) {
		$infos = array();
		$infos['CM'] = 0;
		$infos['TP'] = 0;
		$infos['TD'] = 0;
		$infos['EI'] = 0;

		$prof = Utilisateur::where('id', $id)->first();
		$heures = Heure::where([['ue_id', $ue], ['utilisateur_id', $id]])->get();

		$infos['id'] = $prof->id;
		$infos['prenom'] = $prof->prenom; 
		$infos['nom'] = $prof->nom; 

		foreach ($heures as $heure) {
			$infos[$heure->type] += $heure->volume;
		}

		return $infos;
	}

	private function listeProfNonAffectes($ue) {
		$liste = array();

		$exceptions = array();
		$assos = Association::where('ue_id', $ue)->get();
		foreach ($assos as $asso) {
			array_push($exceptions, $asso->utilisateur_id);
		}

		$profs = Utilisateur::whereNotIn('id', $exceptions)->get();

		$count = 1;
		foreach ($profs as $prof) {
			if ($prof->inscriptionValide == true) {
				$elemProf = array();
				$elemProf['id'] = $prof->id;
				$elemProf['prenom'] = $prof->prenom;
				$elemProf['nom'] = $prof->nom;

				$liste[$count] = $elemProf;
				$count += 1;
			}
		}

		return $liste;
	}

	public function afficherIntervenants(Request $request) {
		if ($this->droitsConformesVision() == false) {
			return redirect()->route('profil');
		}

		$user = Utilisateur::where('email', $_SESSION['email'])->first();

		$listeUEs = array();
		$count = 1;

		// Si RDI, tous les UES sont affichés 


		$UES = Ue::where('utilisateur_id', $user->id)->get();

		foreach ($UES as $UE) {

			$elem = array();
			$elem['id'] = $UE->id;
			$elem['nom'] = $UE->libelle;
			$elem['volume'] = $UE->volumeUE;

			// liste des heures globales effectuées

			$heures = 0;

			$listeHeure = Heure::where([['ue_id', $UE->id], ['valide', true]])->get();
			foreach ($listeHeure as $elemHeure) {
				switch ($elemHeure->type) {
					case "CM": $heures += $elemHeure->volume * 3/2; break;
					case "TD": $heures += ($elemHeure->volume)/$UE->nombreGroupeTD; break;
					case "TP": $heures += ($elemHeure->volume * (2/3))/$UE->nombreGroupeTP; break;
					case "EI": $heures += ($elemHeure->volume * (7/6))/$UE->nombreGroupeEI; break;
				}
			}

			$elem['fait'] = $heures;

			// liste prof affectés 

			$assos = Association::where('ue_id', $UE->id)->get();
			$listeProfsAffecte = array();
			$countProf = 1;

			foreach ($assos as $asso) {											
				$listeProfsAffecte[$countProf] = $this->infoProf($asso->utilisateur_id, $asso->ue_id);
				$countProf += 1;
			}

			$elem['profs'] = $listeProfsAffecte;

			// liste prof non affectés

			$elem['nonProfs'] = $this->listeProfNonAffectes($UE->id);

			$listeUEs[$count] = $elem;
			$count += 1;
		}

		//echo json_encode($listeUEs, JSON_UNESCAPED_UNICODE);
		return view('quotas_UE')->with(['ues' => $listeUEs]);
	}

	public function ajouterIntervenant(Request $request) {
		if ($this->droitsConformesModification($request) == false) {
			//echo "vous n'avez pas les droits";
			return redirect()->route('profil');
		}

		$ue = $request->input("ue");
		$intervenant = $request->input("intervenant");

		if ((Ue::where('id', $ue)->count() > 0) && (Utilisateur::where('id', $intervenant)->count() > 0)) {
			if (Association::where([['ue_id', $ue], ['utilisateur_id', $intervenant]])->count() == 0) {
				Association::create(['ue_id' => $ue, 'utilisateur_id' => $intervenant, 'valide' => true]);
			}
			else {
				//return "L'utilisateur est déjà associé à ce cours";
				return redirect()->route('listeIntervenants');
			}
		}
		else {
			//return 'Utilisateur ou UE inconnue';
			return redirect()->route('listeIntervenants');
		}
                return redirect()->route('listeIntervenants');
	}

	public function supprimerIntervenant(Request $request) {
		if ($this->droitsConformesModification($request) == false) {
			//echo "vous n'avez pas les droits";
			return redirect()->route('profil');
		}

		$ue = $request->input("ue");
		$intervenant = $request->input("intervenant");

		Association::where([['ue_id', $ue], ['utilisateur_id', $intervenant]])->delete();

		return redirect()->route('listeIntervenants');
	}
}













