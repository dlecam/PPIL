<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Repositories\MailRepository;
use App\HeureUE as heureue;

/*

MesCoursController.php, 
créé 20 mai
modifié le 21 mai
Responsable : Steve Maggioli

*/

class MesCoursController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		//
	}

        public function affiche(){
            //décalaration des variable
            $count = 1 ;
            $count2 = 1;
            $results = array();
            $res = array();
            $tableauNotif = array();
            $Notif = array();
            $progression = array();
            $sommeTP=0;
            $sommeTD=0;
            $sommeEI=0;
            $sommeCM=0;
            $totalCM = 0 ;
            $totalTD = 0 ;
            $totalEI = 0 ;
            $totalTP = 0 ;
            $email = $_COOKIE['email'];
            //fin de déclaration des variables ...
            ///////////////////////////////////////
            //recuperation de l'id de l'utilisateur
            $id = DB::select("SELECT id FROM utilisateurs WHERE email = '$email'");
            if($id[0]->id==NULL){//si aucun utilisateur existe a cet email.
                return view('connexion');
            }            
            $user_id = $id[0]->id;
            //recupération des cours ou est inscrit l'utilisateur.
            $cours = DB::table('ue_utilisateur')->where('utilisateur_id', $user_id)->get();
            foreach ($cours as $cour) {//Pour chaque cours
                $res["id"] = $cour->ue_id;
                $ue = DB::table('ues')->where('id',$cour->ue_id)->first();
                if($ue != NULL){//s'il n'y a pas de ue correspondant
                    $res["nom"] = $ue->libelle; // nom de l'ue
                    $res["id"] = $ue->id; //id de l'ue
                    $heures = DB::table('heuresUE')->where('ue_id',$ue->id)->get();
                    foreach($heures as $heure){ // traitement de chaque heure effectué dans ce cours ...
                        if($heure->utilisateur_id == $user_id){
                            if($heure->valide != 1){// si l'heure a ete validée on fait la somme et que l'utilisateur est le bon 
                                $Notif["id"] = $heure->id ; 
                                $Notif["nom"] = $res["nom"] ;
                                $Notif["heure"] = $heure->volume ;
                                $Notif["type"] = $heure->type ;
                                $Notif["date"] = $heure->created_at;
                                $tableauNotif[$count2] = $Notif;
                                $count2 += 1;              
                            }else{//sinon on rempli avec le tableau de en attente de validation
                                switch ($heure->type){
                                    case 'CM' :$sommeCM += $heure->volume; $totalCM += $heure->volume; break;
                                    case 'TD' :$sommeTD += $heure->volume; $totalTD += $heure->volume;break;
                                    case 'TP' :$sommeTP += $heure->volume; $totalTP += $heure->volume;break;
                                    case 'EI' :$sommeEI += $heure->volume; $totalEI += $heure->volume;break;
                                }
                            }
                        }   
                    }
                }
                $res["CM"] = $sommeCM; $sommeCM=0;
                $res["TD"] = $sommeTD; $sommeTD=0;
                $res["TP"] = $sommeTP; $sommeTP=0;
                $res["EI"] = $sommeEI; $sommeEI=0;
                // les heures effectuées .
                // les heures en attente de validation 
                $results[$count] = $res; 
                $count+=1;
            }
            
            //Calcul de la progression totale de l'enseignant
            
            for($i = $count-1 ; $i > 0 ; $i--){
                $totalCM += $results[$i]["CM"];
                $totalTD += $results[$i]["TD"];
                $totalEI += $results[$i]["EI"];
                $totalTP += $results[$i]["TP"];
            }
            $totalEffectue = (($totalCM*3)/2) + (($totalEI*7)/6) + (($totalTP*2)/3) + $totalTD ;
            $tmp = DB::table('utilisateurs')->where('id' ,$user_id)->first();
            $req = DB::table('statuts')->where('id',$tmp->statut_id)->first();
            $volumeAFaireTotal = $req->volumeHoraire;
            $quotas = array();
            $quotas["effetue"]=$totalEffectue/2;
            $quotas["total"]=$volumeAFaireTotal;
            
            //Creation du tableau a retourner ...
            $resultat = array();
            $resultat["cours"] = $results;
            $resultat["notif"] = $tableauNotif;
            $resultat["total"] = $quotas ;
            //return $cours[][] / $notifications[] / $progression[]
            //echo json_encode($results, JSON_UNESCAPED_UNICODE); 
            return view('mesCours', ['results' => $resultat]);
        }
        
     public function ajouterHeure(Request $request){
         $id = $request->input('id');//id de l'ue concernée
         $volume = $request->input('volume'); //nombre d'heure a ajouter
         $type = $request->input('type');//type de cours : CM / TP / TD / EI
         $email = $_COOKIE['email'];
         //Verifs des données
         if($id == NULL | $volume == NULL | $email == NULL | $volume < 1 | !($type != 'CM' | $type != 'EI' | $type != 'TP' | $type != 'TD')){ //vérification des erreurs éventuelles ...
             return redirect()->route('mesCours',['erreur' => "Une erreur s'est produite lors du traitement, veuillez recommencer." ]);
         }
        
         $count = DB::table('utilisateurs')->where('email',$email)->count(); 
         $count_ue = DB::table('ues')->where('id',$id)->count();
         
         if( $count == 0 | $count_ue == 0){ // si l'user ou l'ue n'existe pas
             return redirect()->route('mesCours',['erreur' => "Une erreur s'est produite lors du traitement, veuillez recommencer." ]);
         }
         
         $user = DB::table('utilisateurs')->where('email',$email)->first();
         $user_id = $user->id;
         $inscription = DB::table('ue_utilisateur')->where('ue_id',$id)->get();
         $cpt=0;
         foreach($inscription as $ins){
             if($ins->utilisateur_id == $user_id){
                 $cpt = 1 ;
             }
         }
         if($cpt == 0){
             echo "erreur";
         }
         
         $ue = DB::table('ues')->where('id',$id)->first();
         $total=0;
         $volumeMax = $ue->volumeUE;
         $heures = DB::table('heuresUE')->where('ue_id',$id)->get();
         foreach($heures as $heure){
             switch($heure->type){
                 case 'CM' : $total+= ((($heure->volume)*3)/2); break;
                 case 'TP' : $total+= ((($heure->volume)*2)/3); break;
                 case 'EI' : $total+= ((($heure->volume)*7)/6); break;
                 case 'TD' : $total+= $heure->volume; break;
             } 
         }
         switch($type){
             case 'CM' : $volumeEquivTD= ((($volume)*3)/2); break;
             case 'TP' : $volumeEquivTD= ((($volume)*2)/3); break;
             case 'EI' : $volumeEquivTD= (($volume*7)/6); break;
             case 'TD' : $volumeEquivTD= $volume; break;
         }
         if(($total + $volumeEquivTD) <= ($volumeMax+0.5) ){//verif que cela ne dépasse pas le volume max
             $tuple = new heureue;
             $tuple->ue_id = $id;
             $tuple->utilisateur_id = $user_id;
             $tuple->volume = $volume;
             $tuple->type = $type;
             $Resp = DB::table('ues')->where('id',$id)->first();
             $RespUE = $Resp->utilisateur_id;
             if($user->estRDI == 1 | $RespUE == $user_id){
                 $tuple->valide = 1;
             }
             $tuple->save();
             return $this->affiche();
            
//insertion dans la BDD du tuple heureUE... 
         }else{
             return redirect()->route('mesCours',['erreur' => "Le nombre d'heure n'a pas pu etre ajouté car le quotas est deja atteint pour ce cours et/ou vous avez indiqué trop d'heure." ]);
             //refus de l'insertion ...
         }   
     }
     
     
    public function supprimerHeure(Request $request){
        $id = $request->input('id'); // id de l'heure ue a annuler ...
        $email = $_COOKIE['email'];
        if($id==NULL | $email == NULL){return redirect()->route('mesCours',['erreur' => "Erreur lors du traitement de l'annulation d'heure. Veuillez recommencer"]);} //ERREUR
        $count = DB::table('heuresUE')->where('id',$id)->count();
        $count_user = DB::table('utilisateurs')->where('email',$email)->count() ;
        if($count<1 | $count_user<1 ){return redirect()->route('mesCours',['erreur' => "Erreur lors du traitement de l'annulation d'heure. Veuillez recommencer"]);} //ERREUR
        $user = DB::table('utilisateurs')->where('email',$email)->first();
        $user_id = $user->id;
        $ue = DB::table('heuresUE')->where('id',$id)->first();
        $ue_id = $ue->ue_id;    
        if($ue->valide == 1){
            return redirect()->route('mesCours',['erreur' => "Erreur lors du traitement de l'annulation d'heure. Veuillez recommencer"]);
        }
        DB::table('heuresUE')->where('id',$id)->delete(); // suppression du tuple.
        return redirect()->route('mesCours');
    }
}


?>