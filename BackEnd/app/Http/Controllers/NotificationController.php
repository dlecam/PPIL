<?php

/*

ResearchController.php, 
créé le dimanche 21 mai
modifié le mardi 23 mai
Responsable : Thomas Lemaire

*/

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Repositories\NotificationRepository as Notifications;

class NotificationController extends Controller {
	
	private $notifications;

	public function __construct(Notifications $notifications) {
		$this->notifications = $notifications;
	}
	
	public function showNotificationPage() {
		$notifs = $this->notifications->notificationList();
		return view('page_notification')->with(['notifications' => $notifs]);
	}
	
	public function validerNotification(Request $request) {
		
		$id = $request->input('id');
		$type = $request->input('type');
				
		if ($id != null && $type != null) {
			$this->notifications->acceptNotificationWithIdAndType($id, $type);
		}
		
		return redirect()->route('notifications');
	}
		
	public function refuserNotification(Request $request) {
		
		$id = $request->input('id');
		$type = $request->input('type');
		
		
		if ($id != null && $type != null) {
			$this->notifications->refuseNotificationWithIdAndType($id, $type);
		}
		
		return redirect()->route('notifications');
	}
}