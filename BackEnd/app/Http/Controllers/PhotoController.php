<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Utilisateur;


class PhotoController extends Controller {

    private $rules = ['photo' => 'required|image|dimensions:max_width=200,max_height=200'];


    public function getForm() {
        $chemin = Utilisateur::where('email', $_SESSION['email'])->value('photo');

        return view('modif_photo', [ 'urlPhoto' => $chemin,]);
    }

    public function postForm(Request $request) {

        /* Vérifie que les règles sur le champ photo sont respectées */
        $this->validate($request, $this->rules);

        $image = $request->file('photo');

        if ($image->isValid()) {

            $chemin = 'uploads';
            $extension = $image->getClientOriginalExtension();

            do {
                $nom = str_random(10) . '.' . $extension;
            } while (file_exists($chemin . '/' . $nom));


            if ($image->move($chemin, $nom)) {
                $chemin = $chemin . '/' . $nom;

                /* On met à jour le champ photo de l'utilisateur */
                $user = Utilisateur::where('email', $_SESSION['email'])
                    ->update(['photo' => $chemin,]);

                return view('modif_photo', [ 'urlPhoto' => $chemin, ]);
            }

        }

        return redirect('changePic')
            ->with('error', 'Désolé mais votre image ne peut pas être envoyée !'); 
    }

}