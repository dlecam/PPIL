<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Repositories\MailRepository;
use Illuminate\Support\Facades\Crypt;
use App\Utilisateur;
/*
ProfilController.php, 
créé le 15 mai
modifié le 21 mai
Responsable : Steve Maggioli
*/
class ProfilController extends Controller
{/**
 * Create a new controller instance.
 *
 * @return void
 */
	public function __construct()
	{
		//
	}
	public function changePassword(Request $request, MailRepository $mailer){
		$old = $request->input('oldPassword');
		$new = $request->input('newPassword');
		$newConfirm = $request->input('newPasswordConfirm');
		$email = $_COOKIE['email'];
		$password = DB::select("SELECT motDePasse FROM utilisateurs WHERE email = '$email'");
		$pass = $password[0]->motDePasse;
		if($pass == $old){ // le vieux mdp correspond 
			if($new==$newConfirm){ // le new mdp et sa confirmation sont OK
				if(strlen($new)<8){
					return redirect()->route('changePassword',['erreur' =>'mdp trop court']);
				}
                                $crypter = Crypt::encrypt($newConfirm);
				DB::table('utilisateurs')->where('email', $email)->update(['motDePasse' => $crypter]);//maj du mdp dans la BDD
				setcookie('password', $newConfirm, time() + 7*24*3600, "/", null, false, true); // maj mdp dans le cookie
				//Creation corps du mail de confirmation
				$sujet = "Confirmation modification mot de passe. No Reply !";
				$message_txt = "Bonjour\nVotre mot de passe a bien ete modifier.\n";
				$message_html = "<html><head></head><body><b>Bonjour</b><br/>mail envoye automatiquement, ne pas repondre. <br/>Votre nouveau mot de passe a bien ete modifier .<br/>Si vous n'etes pas a l'origine de ce changement, veuillez contacter l'administrateur.</body></html>";

				$mailer->sendMail($email, $message_txt, $message_html, $sujet); // envoi du mail par l'utilisation du MailRepository .
				return redirect()->route('profil');
			}
		}else{ // mauvais mdp renseigné
			return redirect()->route('changePassword',['erreur' =>'mauvais mdp']);
		}
	}
	public function showPasswordPage() {
		return view('modif_mdp');
	}
	public function showResetPasswordPage() {
		return view('reinit_mdp');
	}
	public function resetPassword(Request $request, MailRepository $mailer) {
		$email = $request->input('email');//recuperation du parametre 'email' dans la requete
		if($email!=NULL){ // si un paramètre est renseigné : 
			$user = DB::table('utilisateurs')->where('email',$email)->first(); // comptage du nombre de cet email dans la BDD
			$count = DB::table('utilisateurs')->where('email',$email)->count();
			if ($count>0) { // si on trouve un utilisateur avec ce mail
				//generation MDP 
				$mdp = $this->genMDP();

				//Modification du password user dans la BDD 
                                $crypter = Crypt::encrypt($mdp);
				DB::table('utilisateurs')->where('email', $email)->update(['motDePasse' => $crypter]);

				//contenu du futur mail a envoyer ... 
				$sujet = "Reinitialisation mot de passe. No Reply !";
				$message_txt = "Bonjour\nVoici votre demande de reinitialisation de mot de passe.\nVotre mot de passe est $mdp";
				$message_html = "<html><head></head><body><b>Bonjour</b><br/>mail envoye automatiquement, ne pas repondre. <br/>Votre nouveau mot de passe est $mdp .<br/>Vous pouvez changer ce mot de passe a tout moment sur votre page de profil.</body></html>";
				//fin contenu du mail.
				$mailer->sendMail($email, $message_txt, $message_html, $sujet); // envoi du mail par l'utilisation du MailRepository .
				return redirect()->route('connexion');
			}else{ // si $email ne correspond a personne dans la BDD 
				return redirect()->route('reinit_mdp',['erreur' =>'email non correspondant']);
			}
		}else{// si $email est vide
			return redirect()->route('reinit_mdp',['erreur' =>'champs email vide']);
		}
	}
	private function genMDP(){ // Fonction de génération d'un mdp alphanum sur 10 char.
		$char = 'aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz00112233445566778899'; 
		$mdp = str_shuffle($char); 
		$mdp = substr($mdp, 4, 10);
		return $mdp;
	}
        public function showProfilPage() {
            //déclaration des variables
            if(isset($_SESSION['email'])){
                $email = $_SESSION['email'];                
            }
            //fin de déclaration des variables
            //vérification si le champs n'est pas vide
            if ($email==NULL) { return view('profil'); }
            //reccupération du nombre d'utilisateur avec cet email
            $count = DB::table('utilisateurs')->where('email',$email)->count();
            if ($count==0) { // Si il n'y a pas d'utilisateur avec cet email
                return view('info_profil');
            }
            //fin des vérifications de l'email dans le $_COOKIE
            //reccupération du tuple correspondant à l'utilisateur connecté
            $user = DB::table('utilisateurs')->where('email',$email)->first();
            //création du tableau à renvoyer à la vue
            $user_array = array();
            $user_array["nom"] = $user->nom; // nom de l'utilisateur
            $user_array["prenom"] = $user->prenom; // prénom de l'utilisateur
            $user_array["email"] = $user->email; // email de l'utilisateur ==$_COOKIE['email']
            $user_array["photo"] = $user->photo; // photo de l'utilisateur
            // reccupération du tuple correspondan au statut de l'utilisateur
            $statut = DB::table('statuts')->where('id',$user->statut_id)->first();
            $user_array["statut"] = $statut->libelle;
            //retour pour la vue
            return view('info_profil',['user' => $user_array]);
        }
        public function modify(Request $request) {
            //déclaration des variables
            //reccupération de l'email actuel de l'utilisateur
            $email = $_SESSION['email'];
            //reccuperation des données du formulaire
            $new_nom = $request->input('newNom');
            $new_prenom = $request->input('newPrenom');
            $new_email = $request->input('newEmail');
            //recupperation du tuple correspondant à l'utilisateur connecté
            $user = DB::table('utilisateurs')->where('email',$email)->first();
            //Fin de la déclaration des variables
			
            if ($email!=$new_email) {// L'adresse email a pas été changée
                //On vérifie si la nouvel n'existe pas déjà dans la DB
                $count_email = DB::table('utilisateurs')->where('email',$new_email)->count();
                if ($count_email > 0) {
                    return redirect()->route('profil');
                }
            }
            //sauvegarde des nouvelles données dans la DB
            
            setcookie('email', $new_email, time() + 7*24*3600,"/",null,false,true);
			Utilisateur::where('email',$email)->update( ['nom' => $new_nom ,
                                                         'prenom' => $new_prenom , 
                                                         'email' => $new_email
                                                        ] );
            return redirect()->route('profil');
        }
}