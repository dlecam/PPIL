<?php
/*

ResearchController.php, 
créé le mercredi 17 mai
modifié le mercredi 17 mai
Responsable : Thomas Lemaire

*/


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Utilisateur as Utilisateur;

class ResearchController extends Controller {

    public function __construct() {}
	
	// Fonction de recherche simple retournant une vue contenant les personnes correspondante  à la recherche
	
	public function showPage() {
		return view('annuaire');
	}
	
	public function searchPerson(Request $request) {
		if ($request->input('search') != null) {
			$results = array();
			$terms = $request->input('search');
			
			$matches = Utilisateur::where(DB::raw("CONCAT(CONCAT(prenom, ' '), nom)"), 'LIKE', '%'.$terms.'%')->orWhere(DB::raw("CONCAT(CONCAT(nom, ' '), prenom)"), 'LIKE', '%'.$terms.'%')->get();
			
			$count = 1;
			foreach ($matches as $user) {
				if ($user->inscriptionValide == true) {	
					$results[$count] = array();
					$results[$count]["prenom"] = $user->prenom;
					$results[$count]["nom"] = $user->nom;
					$results[$count]["email"] = $user->email;

					if ($user->photo == null || $user->photo == "") {
						$results[$count]["photo"] = null;
					}
					else {
						$results[$count]["photo"] = $user->photo;
					}

					$count += 1;
				}					
			}

            echo json_encode($results, JSON_UNESCAPED_UNICODE);
		}		
    }
}