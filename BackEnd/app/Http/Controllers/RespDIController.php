<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Utilisateur;


/*

ChangeStatutController.php, 
créé 24 mai
modifié le 24mai
Responsable : Steve Maggioli

*/

class RespDIController extends Controller {
    /**
     * Create a new controlleur instance.
     * 
     * @return void
     */
    
    public function __construct() {
        
    }
    /**
     * Fonction qui retourne la somme des heures effectuées par un utilisateur
     * @param type $id
     */
    public function heureFaite($id) {
        $heuresEffectuees = DB::table('heuresue')->where('utilisateur_id',$id)->get();
        $somme = 0;
        foreach ($heuresEffectuees as $heureEffectuee) {
            if ($heureEffectuee->valide == 1 ) {
                $somme += $heureEffectuee->volume;
            }
        }
        return $somme;
    }
    
    public function showDIPage() {
        //déclaration des varariables
        $email = $_COOKIE['email'];
        
        //fin de déclaration des variables
        //vérification si le champs n'est pas vide
        if ($email==NULL) { return redirect()->route('profil'); }
        //reccupération du nombre d'utilisateur avec cet email
        $count = DB::table('utilisateurs')->where('email',$email)->count();
        if ($count==0) { // Si il n'y a pas d'utilisateur avec cet email
            return redirect()->route('profil');
        }
        $user = DB::table('utilisateurs')->where('email',$email)->first();        
        $estRDI = $user->estRDI;
        if ($estRDI!=1 ) { // Vérifie si l'utilisateur est bien un responsable du département informatique
            return redirect()->route('profil');
        }
        //fin des vérifications
        $user_array = array();
        $cpt = 1;
        $user_array[$cpt] = array();
        
        $users = DB::table('utilisateurs')->get(); // Récupère tous les utilisateurs du site
        foreach ($users as $utilisateur) {
            if ($email != $utilisateur->email) { // On ne récupère pas le RDI
                $user_array[$cpt]["nom"] = $utilisateur->nom; //nom de l'enseignant
                $user_array[$cpt]["prenom"] = $utilisateur->prenom; // prenom de l'enseignant
                $statut = DB::table('statuts')->where('id',$utilisateur->statut_id)->first();
                $user_array[$cpt]["statut"] = $statut->libelle; // Statut de l'enseignant
                $user_array[$cpt]["heureAFaire"] = $statut->volumeHoraire;
                $user_array[$cpt]["heureFaite"] = $this->heureFaite($utilisateur->id);
                
                // liste pour les statuts restants
                $statutRestant = array();
                $cpt2 = 1;
                $statutRestant[$cpt2] = array();
                $liste_statuts = DB::table('statuts')->get();
                foreach ($liste_statuts as $liste_statut) {
                    if ($liste_statut->id != $statut->id) { // on ne reprend pas le statut actuel de l'utilisateur
                        $statutRestant[$cpt2] = $liste_statut->libelle;
                        $cpt2 += 1;
                    }                    
                }
                $user_array[$cpt]["statutRestant"] = $statutRestant;
                $user_array[$cpt]["id"] = $utilisateur->id;
                $cpt += 1;
            }
        }
        return view('pageDI',['users' => $user_array]);
    }
    
    
    public function modifStatut(Request $request) {
        //vérification des données
        //vérifier si l'id correspond bien à un utilisateur
        $count = DB::table('utilisateurs')->where('id',$request->input('id'))->count();
        
        if ($count<1) {
            //return view('pageDI',['erreur' => "erreur lors de la modification de statut d'un enseignant"]);
            return $this->showDIPage();
        }
        // vérifier si le statut reçut est bien dans le base
        $statutCount = DB::table('statuts')->where('libelle',$request->input('statut'))->count();
        if ($statutCount < 1) {
            //return view('pageDI',['erreur' => "erreur lors de la modification de statut d'un enseignant"]);
            return $this->showDIPage();
        }
        // regarder si le statut a été modifier avant de modifier dans la DB
        $statut = DB::table('statuts')->where('libelle',$request->input('statut'))->first(); // récupérer le bon statut
        $user = DB::table('utilisateurs')->where('id',$request->input('id'))->first(); // récupérer le bon utilisateur
        if ($statut->id==$user->statut_id) {
            //return view('pageDI',['erreur' => "Le statut n'a pas été changé"]);
            return $this->showDIPage();
        }
        //fin des vérifications des données
        //Enregistrement du nouveau statut dans la DB
        $statut = DB::table('statuts')->where('libelle',$request->input('statut'))->first(); // récupérer le bon statut
        Utilisateur::where('id',$user->id)->update(['statut_id' => $statut->id]);
        
        return $this->showDIPage();
    }
}