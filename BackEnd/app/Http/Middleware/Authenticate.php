<?php

/*

Authenticate.php, 
modifié le mardi 23 mai
Responsable : Thomas Lemaire

*/

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Utilisateur as Utilisateur;
use App\Repositories\NotificationRepository as Notifications;
use App\Repositories\UeCalculator as UeCalculator;
use App\Repositories\RightCalculator as RightCalculator;
use Illuminate\Contracts\Encryption\DecryptException;


class Authenticate {
	
    public function handle($request, Closure $next) {
		
		$fromCookie = false;
		
		if ($request->input('email') != null && $request->input('password') != null) {
			$email = $request->input('email');
			$password = $request->input('password');
		}

		else if (isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
			$email = $_COOKIE['email'];
			$password = $_COOKIE['password'];
			$fromCookie = true;
		}
		
		// Décryptage du mot de passe
		if (isset($password) && $fromCookie) {
			try {
				$password = Crypt::decrypt($password);
			} catch (DecryptException $e) {
				// Erreur de décryptage, coupure du service
				unset($password);
				unset($email);
			}
		}
		
		$erreur = false;
		
		if (isset($email) && isset($password)) {
			$erreur = true; // si cette partie echoue, c'est forcement qu'il y a une erreur ID/pass, sinon la route est lancée et cette variable n'est pas utilisée.
			$user = Utilisateur::where('email', $email)->first();
			
			if ($user != null && $user->inscriptionValide) {
				$decrypted = Crypt::decrypt($user->motDePasse);
				
				// Si l'utilisateur peut se connecter
				if ($decrypted == $password) {
					session_start();
					$_SESSION['email'] = $email;
					setcookie('email', $email, time() + 7*24*3600, "/", null, false, true);
					setcookie('password', Crypt::encrypt($password), time() + 7*24*3600, "/", null, false, true);

					$user = Utilisateur::where('email', $email)->first();
					$_SESSION["FULL_NAME"] = $user->prenom." ".$user->nom;
					
					if ($user->photo != null && $user->photo != "") {
						$_SESSION["PHOTO"] = $user->photo;
					}

					// Création de la classe notification et update du nombre
					$notifications = new Notifications();
					$ueCounter = new UeCalculator();
					$rights = new RightCalculator();

					return $next($request);
				}
			}
		}
		
		// Sinon, nettoyer les cookies et la session, et rediriger vers l'écran de connexion
		
		if (session_status() == PHP_SESSION_ACTIVE) {
			unset($_SESSION['email']);
			session_destroy();
		}
		setcookie('email', null, -1, "/", null, false, true);
		setcookie('password', null, -1, "/", null, false, true);
		
		if ($erreur) {
			return redirect()->route('connexion', ["erreur" => "1"]);
		}
		else {
			return redirect()->route('connexion');
		}
    }
}
