<?php

namespace App\Repositories;

/*

MailRepository.php, 
créé 15 mai
modifié le 23 mai
Responsable : Steve Maggioli

*/
class MailRepository
{

    protected $user;

    public function __construct()
	{
	}

        
    public function sendMail($mail, $msgTxt, $msgHtml, $subject){ // Fonction venant du site openclassroom : https://openclassrooms.com/courses/e-mail-envoyer-un-e-mail-en-php
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
        	$passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //=====Déclaration des messages au format texte et au format HTML.
        $message_txt = $msgTxt;
        $message_html = $msgHtml; 
        //==========
 
        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========
 
        //=====Définition du sujet.
        $sujet = $subject;
        //=========
 
        //=====Création du header de l'e-mail.
        $header = "From: \"ServiceEnseignant\"<postmaster@localhost>".$passage_ligne;
        $header.= "Reply-to: \"ServiceEnseignant\" <postmaster@localhost>".$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========
 
        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format texte.
        $message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_txt.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========
 
        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }

}