<?php

/*

NotificationRepository.php, 
créé le dimanche 21 mai
modifié le mardi 23 mai
Responsable : Thomas Lemaire

*/

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Utilisateur as User;
use App\HeureUE as Heure;
use App\UeUtilisateur as InscriptionUE;
use App\Ue as Ue;

class NotificationRepository {
	
    protected $user;
	protected $mailler;
	protected $notifications;
	
    public function __construct() {
		$this->user = User::where('email', $_SESSION['email'])->first();
		$this->mailler = new MailRepository();
		$this->updateNotifications();
	}
	
	private function userDefined() {
		return $this->user != null;
	}
	
	private function notificationBelongsToUser($notification_ID, $type) {
		// vérifier que la notif appartient bien à la personne
		
		if ($type == "Inscription" && $this->user->estRDI == true) {
			return true;
		}
		else {
			if ($type == 'InscriptionUE') {
				$inscriptionUE = InscriptionUE::where('id', $notification_ID)->first();
				$ue = $inscriptionUE->ue()->first();
				if ($ue->utilisateur_id == $this->user->id) {
					return true;
				}
			}
			else if ($type == 'HeureUE') {
				$heureUE = Heure::where('id', $notification_ID)->first();
				$ue = $heureUE->ue()->first();
				if ($ue->utilisateur_id == $this->user->id) {
					return true;
				}
			}
		}
		return false;
	}

    public function updateNotifications() {
		$this->notifications = $this->notificationList();
		$_SESSION["NOTIFICATION_COUNT"] = count($this->notifications);
	}
        
    public function notificationList() {
				
		if ($this->userDefined()) {
			
			$notifications = array();
			$count = 1;
			
			// Demandes d'inscriptions spécifiques au RDI
			
			if ($this->user->estRDI) {
				$inscriptions = User::where('inscriptionValide', false)->get();
				
				foreach ($inscriptions as $inscription) {
										
					$notif = array();
					$notif["type"] = "Inscription";
					$notif["id"] = $inscription->id;
					$notif["nomComplet"] = $inscription->prenom." ".$inscription->nom;
					$notif["message"] = $notif["nomComplet"]." a fait une demande d'incription";
					$notif["date"] = "";
					if ($inscription->created_at != null) {
						$notif["date"] = "Le ".$inscription->created_at->format("j.m.Y à H:i");
					}
						
					$notifications[$count] = $notif;
					$count++;
				}
			}
			
			// Demandes d'inscription spécifiques au responsable d'UE
			
			$ues = Ue::where('utilisateur_id', $this->user->id)->get();
			
			foreach ($ues as $ue) {
				$inscriptions = InscriptionUE::where([['ue_id', $ue->id], ['valide', false]])->get();
				
				foreach ($inscriptions as $inscription) {
					$notif = array();
					
					$demandant = $inscription->utilisateur()->first();
					$notif["type"] = "InscriptionUE";
					$notif["id"] = $inscription->id;
					$notif["nomComplet"] = $demandant->prenom." ".$demandant->nom;
					$notif["nomCours"] = $ue->libelle;
					$notif["message"] = $notif["nomComplet"]." a fait une demande d'incription pour le cours ".$notif["nomCours"];
					$notif["date"] = "";
					if ($inscription->created_at != null) {
						$notif["date"] = "Le ".$inscription->created_at->format("j.m.Y à H:i");
					}
				
					$notifications[$count] = $notif;
					$count++;
				}
			}
			
			// Demandes d'interventions spécifiques au responsable d'UE
			
			$ues = Ue::where('utilisateur_id', $this->user->id)->get();
			
			foreach ($ues as $ue) {
				$heures = Heure::where([['ue_id', $ue->id], ['valide', false]])->get();
				
				foreach ($heures as $heure) {
					$notif = array();
					
					$demandant = $heure->utilisateur()->first();
					$notif["type"] = "HeureUE";
					$notif["id"] = $heure->id;
					$notif["nomComplet"] = $demandant->prenom." ".$demandant->nom;
					$notif["nomCours"] = $ue->libelle;
					$notif["volume"] = $heure->volume;
					$notif["typeCours"] = $heure->type;
					$notif["message"] = $notif["nomComplet"]." a demandé à ajouter ".$heure->volume."h de ".$heure->type." pour le cours ".$notif["nomCours"];
					$notif["date"] = "";
					if ($heure->created_at != null) {
						$notif["date"] = "Le ".$heure->created_at->format("j.m.Y à H:i");
					}
				
					$notifications[$count] = $notif;
					$count++;
				}
			}
			
			return $notifications;
		}
	}
	
	public function acceptNotificationWithIdAndType($notification_ID, $type) {
		if ($this->userDefined() && $this->notificationBelongsToUser($notification_ID, $type)) {
			
			// Valider une inscription 
						
			if ($type == 'Inscription') {
				
				$user = User::where('id', $notification_ID)->first();

				if ($user != null) {
					$user->inscriptionValide = true;
					$user->save();
				}
			}
			
			// Valider une inscription UE
			
			else if ($type == 'InscriptionUE') {
				$inscriptionUE = InscriptionUE::where('id', $notification_ID)->first();

				if ($inscriptionUE != null) {
					$inscriptionUE->valide = true;
					$inscriptionUE->save();
				}
			}
			
			// Valider une heure UE
			
			else if ($type == 'HeureUE') {
				$heure = Heure::where('id', $notification_ID)->first();

				if ($heure != null) {
					$heure->valide = true;
					$heure->save();
				}
			}
		}
	}
	
	public function refuseNotificationWithIdAndType($notification_ID, $type) {
		if ($this->userDefined() && $this->notificationBelongsToUser($notification_ID, $type)) {
			
			// Valider une inscription 
						
			if ($type == 'Inscription') {
				
				$user = User::where('id', $notification_ID)->first();

				if ($user != null) {
					if ($user->inscriptionValide == false) {
						$user->delete();
					}
				}
			}
			
			// Valider une inscription UE
			
			else if ($type == 'InscriptionUE') {
				$inscriptionUE = InscriptionUE::where('id', $notification_ID)->first();

				if ($inscriptionUE != null) {
					if ($inscriptionUE->valide == false) {
						$inscriptionUE->delete();
					}
				}
			}
			
			// Valider une heure UE
			
			else if ($type == 'HeureUE') {
				$heure = Heure::where('id', $notification_ID)->first();

				if ($heure != null) {
					if ($heure->valide == false) {
						$heure->delete();
					}
				}
			}
		}
	}
}