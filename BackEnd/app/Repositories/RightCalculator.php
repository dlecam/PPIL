<?php

/*

UeCalculator.php, 
créé le dimanche 21 mai
modifié le mardi 23 mai
Responsable : Thomas Lemaire

*/

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Utilisateur as User;
use App\FormationUtilisateur as FormationUtilisateur;
use App\Ue as Ue;

class RightCalculator {
	
    protected $user;
	
    public function __construct() {
		$this->user = User::where('email', $_SESSION['email'])->first();
		if ($this->user != null) {
			$this->updateInfos();
		}
	}
	
	private function updateInfos() {
		
		$formations = FormationUtilisateur::where('utilisateur_id', $this->user->id)->count();
		$ues = Ue::where('utilisateur_id', $this->user->id)->count();
		
		$_SESSION['RDI'] = $this->user->estRDI;
		$_SESSION['RF'] = $formations > 0;
		$_SESSION['RUE'] = $ues > 0;
	}
}
