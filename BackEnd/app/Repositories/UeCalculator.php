<?php

/*

UeCalculator.php, 
créé le dimanche 21 mai
modifié le mardi 23 mai
Responsable : Thomas Lemaire

*/

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Utilisateur as User;
use App\HeureUE as Heure;
use App\Ue as Ue;

class UeCalculator {
	
    protected $user;
	
    public function __construct() {
		$this->user = User::where('email', $_SESSION['email'])->first();
		if ($this->user != null) {
			$this->updateNumbers();
		}
	}
	
	private function updateNumbers() {
		
		$UES = Ue::where('utilisateur_id', $this->user->id)->get();
		
		$total = 0;
		$effectuees = 0;

		foreach ($UES as $UE) {
			
			// liste des heures globales effectuées

			$heures = 0;

			$listeHeure = Heure::where([['ue_id', $UE->id], ['valide', true]])->get();
			foreach ($listeHeure as $elemHeure) {
				switch ($elemHeure->type) {
					case "CM": $heures += $elemHeure->volume * 3/2; break;
					case "TD": $heures += ($elemHeure->volume)/$UE->nombreGroupeTD; break;
					case "TP": $heures += ($elemHeure->volume * (2/3))/$UE->nombreGroupeTP; break;
					case "EI": $heures += ($elemHeure->volume * (7/6))/$UE->nombreGroupeEI; break;
				}
			}

			if ($heures >= ($UE->volumeUE - 2)) {
				$effectuees += 1;
			}
			
			$total += 1;
		}
		
		$_SESSION['UE_TOTAL'] = $total;
		$_SESSION['UE_OK'] = $effectuees;
	}
}

