<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statut extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     	'libelle', 'volumeHoraire'
    ];


    /**
     * recupère les utilisateurs qui possèdent ce statut.
     */
    public function utilisateurs() {
        return $this->hasMany('App\Utilisateur');
    }

}