<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ue extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     	'libelle', 'description', 'volumeUE', 'nombreGroupeTD', 'nombreGroupeTP', 'nombreGroupeEI', 'utilisateur_id', 'formation_id'
	];
    


    /**
     * recupère la formation qui contient cette UE.
     */
    public function formation() {
    	return $this->belongsTo('App\Formation');
    }


    /**
     * recupère le responsable qui gère cette UE.
     */
    public function responsable() {
    	return $this->belongsTo('App\Utilisateur');
    }

}