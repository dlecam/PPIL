<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UeUtilisateur extends Model {

	protected $table = 'ue_utilisateur';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ue_id', 'utilisateur_id', 'valide'];

	
	
	 /**
     * recupère l'utilisateur qui fait la demande d'inscription pour une UE.
     */
    public function utilisateur() {
        return $this->belongsTo('App\Utilisateur');
    }


    /**
     * recupère l'UE concernée par cette demande d'intervention.
     */
    public function ue() {
        return $this->belongsTo('App\Ue');
    }
}