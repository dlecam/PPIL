<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model 
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
     	'nom', 'prenom', 'photo', 'email', 'motDePasse', 'estRDI', 'inscriptionValide', 'statut_id'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'motDePasse'
    ];


    /**
     * recupère le statut que possède l'utilisateur.
     */
    public function statut() {
        return $this->belongsTo('App\Statut');
    }


    /**
     * recupère les formations qu'un responsable gère.
     */
    public function formations() {
        return $this->belongsToMany('App\Formation');
    }



    /**
     * recupère les enseignements d'un utilisateur dont il est responsable.
     */
    public function ues() {
        return $this->hasMany('App\Ue');
    }


    /**
     * recupère les demandes inscriptions d'un utilisateur.
     */
    public function ueutilisateurs() {
        return $this->hasMany('App\UeUtilisateur');
    }

}