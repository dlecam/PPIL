-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 26 mai 2017 à 00:46
-- Version du serveur :  10.1.22-MariaDB
-- Version de PHP :  7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `baseppil`
--

-- --------------------------------------------------------

--
-- Structure de la table `demandeinscriptions`
--

CREATE TABLE `demandeinscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `statutDemande` enum('ACCEPTEE','ANNULEE','EN ATTENTE','REFUSEE') COLLATE utf8_unicode_ci DEFAULT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `demandeinscriptions`
--

INSERT INTO `demandeinscriptions` (`id`, `statutDemande`, `utilisateur_id`, `created_at`, `updated_at`) VALUES
(2, 'EN ATTENTE', 8, '2017-05-25 20:17:48', '2017-05-25 20:17:48'),
(3, 'EN ATTENTE', 9, '2017-05-25 22:10:41', '2017-05-25 22:10:41');

-- --------------------------------------------------------

--
-- Structure de la table `formations`
--

CREATE TABLE `formations` (
  `id` int(10) UNSIGNED NOT NULL,
  `formation` enum('LICENCE','MASTER','DOCTORAT') COLLATE utf8_unicode_ci NOT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `annee` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `formations`
--

INSERT INTO `formations` (`id`, `formation`, `libelle`, `annee`, `created_at`, `updated_at`) VALUES
(1, 'LICENCE', '2017', '1', NULL, NULL),
(2, 'LICENCE', '2017', '2', '2017-05-25 22:00:00', '2017-05-25 22:00:00'),
(3, 'LICENCE', '2017', '3', '2017-05-25 22:00:00', '2017-05-25 22:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `formation_utilisateur`
--

CREATE TABLE `formation_utilisateur` (
  `id` int(10) UNSIGNED NOT NULL,
  `formation_id` int(10) UNSIGNED NOT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `formation_utilisateur`
--

INSERT INTO `formation_utilisateur` (`id`, `formation_id`, `utilisateur_id`, `created_at`, `updated_at`) VALUES
(1, 1, 9, '2017-05-24 22:00:00', '2017-05-24 22:00:00'),
(2, 2, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `heuresue`
--

CREATE TABLE `heuresue` (
  `id` int(10) UNSIGNED NOT NULL,
  `ue_id` int(10) UNSIGNED NOT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `volume` tinyint(3) UNSIGNED NOT NULL,
  `type` enum('CM','TD','TP','EI') COLLATE utf8_unicode_ci NOT NULL,
  `valide` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_05_18_140922_create_statuts_table', 1),
(2, '2017_05_18_140949_create_utilisateurs_table', 1),
(3, '2017_05_18_141047_create_formations_table', 1),
(4, '2017_05_18_141110_create_ues_table', 1),
(5, '2017_05_18_141310_create_ue_utilisateur_table', 1),
(6, '2017_05_18_141335_create_formation_utilisateur_table', 1),
(7, '2017_05_18_141452_create_demandeInscriptions_table', 1),
(8, '2017_05_20_195257_create_heure_UE_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `statuts`
--

CREATE TABLE `statuts` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `volumeHoraire` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `statuts`
--

INSERT INTO `statuts` (`id`, `libelle`, `volumeHoraire`, `created_at`, `updated_at`) VALUES
(1, 'Enseignant Chercheur', 192, NULL, NULL),
(2, 'Doctorant', 64, '2017-05-24 22:00:00', '2017-05-24 22:00:00'),
(3, 'ATER', 192, '2017-05-24 22:00:00', '2017-05-24 22:00:00'),
(4, 'PRAG', 384, '2017-05-24 22:00:00', '2017-05-24 22:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `ues`
--

CREATE TABLE `ues` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `volumeUE` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `nombreGroupeTD` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `nombreGroupeTP` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `nombreGroupeEI` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `utilisateur_id` int(10) UNSIGNED DEFAULT NULL,
  `formation_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `ues`
--

INSERT INTO `ues` (`id`, `libelle`, `description`, `volumeUE`, `nombreGroupeTD`, `nombreGroupeTP`, `nombreGroupeEI`, `utilisateur_id`, `formation_id`, `created_at`, `updated_at`) VALUES
(1, 'math', '', 50, 1, 1, 1, 9, 1, NULL, NULL),
(2, 'optimisation', '', 100, 3, 2, 4, 9, 3, '2017-05-25 22:00:00', '2017-05-25 22:00:00'),
(3, 'modelisation', '', 200, 2, 1, 1, 9, 2, NULL, NULL),
(4, 'cryptographie', '', 100, 2, 4, 2, 9, 2, '2017-05-25 22:00:00', '2017-05-25 22:00:00'),
(5, 'PPIL', '', 150, 3, 2, 2, 9, 2, '2017-05-25 22:00:00', '2017-05-25 22:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `ue_utilisateur`
--

CREATE TABLE `ue_utilisateur` (
  `id` int(11) NOT NULL,
  `ue_id` int(10) UNSIGNED NOT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `valide` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `ue_utilisateur`
--

INSERT INTO `ue_utilisateur` (`id`, `ue_id`, `utilisateur_id`, `valide`, `created_at`, `updated_at`) VALUES
(3, 1, 8, 1, '2017-05-25 22:26:18', '2017-05-25 22:26:18');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `motDePasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estRDI` tinyint(1) NOT NULL DEFAULT '0',
  `inscriptionValide` tinyint(1) NOT NULL DEFAULT '0',
  `statut_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `nom`, `prenom`, `photo`, `email`, `motDePasse`, `estRDI`, `inscriptionValide`, `statut_id`, `created_at`, `updated_at`) VALUES
(8, 'lecam', 'doriane', '', 'doriane.lecam@live.com', 'eyJpdiI6Ikt4c1ZcL1dZdlFUQ3p1bU1ISFBWeU5nPT0iLCJ2YWx1ZSI6InVuYVwvMWs3dDF4V2grMzd6RDZxOE53PT0iLCJtYWMiOiIwMzM4OTM0ZDk2NTgzMDA4NDlkMmFkY2E5MDYxMjRmYTdlZGMwYmU0OWJmOTM1ZmRlMDE5NzkzNjkwNGMzNmRkIn0=', 0, 0, 1, '2017-05-25 20:17:48', '2017-05-25 20:22:25'),
(9, 'Constant', 'Mathieu', '', 'serv.enseignant@gmail.com', 'eyJpdiI6IjR5S3hDaUxRQThCbllNTDlNeFBCWmc9PSIsInZhbHVlIjoiS2xXdDJaZnUySmxjelF3K2RQRGxUWGhaM05PcXNXWVY5MXM0TVNOamF3MD0iLCJtYWMiOiI3YWY5MTEyZWVmN2U4N2UzN2M2YzA2NDg4YTBhOTk1ZWY0MTVmMzM2OGM2ZmQwNGQwNDUyNjY1MDNjMzk5ZDAyIn0=', 1, 1, 2, '2017-05-25 22:10:41', '2017-05-25 22:19:55');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `demandeinscriptions`
--
ALTER TABLE `demandeinscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `demandeinscriptions_utilisateur_id_index` (`utilisateur_id`);

--
-- Index pour la table `formations`
--
ALTER TABLE `formations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formation_utilisateur`
--
ALTER TABLE `formation_utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formation_utilisateur_formation_id_index` (`formation_id`),
  ADD KEY `formation_utilisateur_utilisateur_id_index` (`utilisateur_id`);

--
-- Index pour la table `heuresue`
--
ALTER TABLE `heuresue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `heuresue_ue_id_index` (`ue_id`),
  ADD KEY `heuresue_utilisateur_id_index` (`utilisateur_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `statuts`
--
ALTER TABLE `statuts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ues`
--
ALTER TABLE `ues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ues_utilisateur_id_foreign` (`utilisateur_id`),
  ADD KEY `ues_formation_id_index` (`formation_id`);

--
-- Index pour la table `ue_utilisateur`
--
ALTER TABLE `ue_utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ue_utilisateur_ue_id_index` (`ue_id`),
  ADD KEY `ue_utilisateur_utilisateur_id_index` (`utilisateur_id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `utilisateurs_email_unique` (`email`),
  ADD KEY `utilisateurs_statut_id_foreign` (`statut_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `demandeinscriptions`
--
ALTER TABLE `demandeinscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `formations`
--
ALTER TABLE `formations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `formation_utilisateur`
--
ALTER TABLE `formation_utilisateur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `heuresue`
--
ALTER TABLE `heuresue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `statuts`
--
ALTER TABLE `statuts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `ues`
--
ALTER TABLE `ues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `ue_utilisateur`
--
ALTER TABLE `ue_utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `demandeinscriptions`
--
ALTER TABLE `demandeinscriptions`
  ADD CONSTRAINT `demandeinscriptions_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formation_utilisateur`
--
ALTER TABLE `formation_utilisateur`
  ADD CONSTRAINT `formation_utilisateur_formation_id_foreign` FOREIGN KEY (`formation_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formation_utilisateur_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `heuresue`
--
ALTER TABLE `heuresue`
  ADD CONSTRAINT `heuresue_ue_id_foreign` FOREIGN KEY (`ue_id`) REFERENCES `ues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `heuresue_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ues`
--
ALTER TABLE `ues`
  ADD CONSTRAINT `ues_formation_id_foreign` FOREIGN KEY (`formation_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ues_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `ue_utilisateur`
--
ALTER TABLE `ue_utilisateur`
  ADD CONSTRAINT `ue_utilisateur_ue_id_foreign` FOREIGN KEY (`ue_id`) REFERENCES `ues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ue_utilisateur_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateurs_statut_id_foreign` FOREIGN KEY (`statut_id`) REFERENCES `statuts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
