<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('utilisateurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 50);
            $table->string('prenom', 50);
            $table->string('photo', 255)->nullable();
            $table->string('email', 100)->unique();
            $table->string('motDePasse', 255);
            $table->boolean('estRDI')->default(false);
            $table->boolean('inscriptionValide')->default(false);
            $table->unsignedInteger('statut_id')->nullable();
            $table->timestamps();

            $table->foreign('statut_id')->references('id')->on('statuts')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateurs');
    }
}
