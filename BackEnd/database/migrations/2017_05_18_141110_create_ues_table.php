<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('ues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle', 100);
            $table->text('description');
            $table->unsignedSmallInteger('volumeUE')->default(0);
            $table->unsignedTinyInteger('nombreGroupeTD')->default(1);
            $table->unsignedTinyInteger('nombreGroupeTP')->default(1);
            $table->unsignedTinyInteger('nombreGroupeEI')->default(1);
            $table->unsignedInteger('utilisateur_id')->nullable();
            $table->unsignedInteger('formation_id')->index();
            $table->timestamps();

            $table->foreign('formation_id')->references('id')->on('formations')->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('utilisateur_id')->references('id')->on('utilisateurs')->onDelete('set null')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ues');
    }
}
