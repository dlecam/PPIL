<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUeUtilisateurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ue_utilisateur', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ue_id')->index();
            $table->unsignedInteger('utilisateur_id')->index();
			$table->boolean('valide')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('ue_id')->references('id')->on('ues')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('utilisateur_id')->references('id')->on('utilisateurs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('ue_utilisateur');
    }
}
