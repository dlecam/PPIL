<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandeInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandeInscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('statutDemande', ['ACCEPTEE', 'ANNULEE', 'EN ATTENTE', 'REFUSEE'])->nullable();
            $table->unsignedInteger('utilisateur_id')->index();
            $table->timestamps();
            
            $table->foreign('utilisateur_id')->references('id')->on('utilisateurs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandeInscriptions');
    }
}
