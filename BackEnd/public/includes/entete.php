<!-- FICHIER D'ENTETE A INCLUDE DANS CHACUNE DES VUES -->

<!--  
Créé le : 12/05/2017
Modifié le : 24/05/2017
Responsable : Clément KOCH
-->

<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>PPIL</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/styles.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <script>
            function colorier(champ, erreur) {
                if (erreur)
                    champ.style.color = "red";
                else
                    champ.style.color = "";
            }
            
            function verifMail(champ) {
                var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
                if(!regex.test(champ.value)) {
                    colorier(champ, true);
                    return false;
                } else {
                    colorier(champ, false);
                    return true;
                }
            }

            function cacherAfficher(calque){
                var el = document.getElementById(calque);
                el.style.display = (!el.style.display || el.style.display == "none") ? "block" : "none";
            }

            function afficherAnnuaire(photo, nom, prenom, mail){
                var el = document.getElementById("fiche");
                el.style.display = "block";
                el.innerHTML = '<div class="row"><div class="col-sm-4 col-sm-offset-3 photoProfil fondGris" id="photoPro"></div></div><div class="row"><div class="col-sm-12"><div class="gras titre2 decalageHaut1 texteNoir alignementCentre">'+nom.toUpperCase()+'</div><div class="titre2 alignementCentre">'+prenom+'</div><div class="titre4 italique decalageHaut1 decalageHautBas alignementCentre">'+mail+'</div></div></div>';
            }

            function cacherAnnuaire(){
                var el = document.getElementById("fiche");
                el.style.display ="none";
            }

            function getXhr() {
        
                var xhr = null;
                if(window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest();
                } else if(window.ActiveXObject) {
                    try { 
                    xhr = new ActiveXObject('Msxml2.XMLHTTP');
                    } catch (e) {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                } else {
                    alert('Votre navigateur ne supporte pas les objets XMLHTTPRequest...');
                    xhr = false;
                }
                return xhr;
            }

            function recherche(){  
                var xhr = getXhr();
                
                xhr.addEventListener('readystatechange', function() {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        var chaine = xhr.responseText;
                        var el = document.getElementById("resultats");
                        el.innerHTML = "";
                        var fond = 'fondBlanc';
                        cacherAnnuaire();
                        if(chaine.length > 2) {
                            try {
                              var resultat = JSON.parse(chaine);
                            } catch (e) {
                              console.error("Parsing error:", e); 
                            }
                            for(var i in resultat) {
                                if(fond.length == 8){
                                    fond = 'fondBlanc';
                                } else {
                                    fond = 'fondGris';
                                }
                                 el.innerHTML += '<a onclick="afficherAnnuaire(\''+resultat[i]['photo']+'\',\''+resultat[i]['nom']+'\',\''+resultat[i]['prenom']+'\',\''+resultat[i]['email']+'\')"><div class="row"><div class="col-sm-12 '+fond+' titre4">'+resultat[i]['nom']+' '+resultat[i]['prenom']+'</div></div></a>';
                            }
                        }
                    }
                });
    
                xhr.open('POST','annuaire');
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.send("search="+document.getElementById("rechercher").value);
            }       

        </script>
  </head>
  <body>
    <div class="container-fluid">
        <div class="entete fondGris gras">
            <div class="row ligneEntete">
                <div class="col-xs-2 telephone">
                    <div onclick="cacherAfficher('aCacher')" class="boutonMenuDeroulant">MENU</div>
                </div>
                <div class="col-sm-2 col-sm-offset-0 col-xs-5 ligneBouton">
                    <div class="bouton fondBleu">Exporter</div>
                </div>
                <div class="col-sm-10 col-xs-10"><?php echo $_SESSION["FULL_NAME"]?></div>
            </div>
        </div>

<?php require_once "panneauLateral.php" ?>
