<!-- FICHIER DU PANNEAU LATERAL A INCLUDE DANS CHACUNE DES VUES -->

<!--  
Créé le : 12/05/2017
Modifié le : 17/05/2017
Responsable : Clément KOCH
-->

	<div class="fondGris panneauLateral container-fluid" id="aCacher">

		<!-- Premier bloc de boutons du menu -->
		<div class="row blocMenuHaut blocMenu">
			<div class="col-sm-12 fondBlanc">
				<a href="notifications">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">
									Notifications <div class="nombreMenu fondOrange"><?php echo $_SESSION["NOTIFICATION_COUNT"] ?></div>
								</div>
							</div>
						</div>
					</div>
				</a>
				<a href="annuaire">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">Annuaire</div>
								
							</div>
						</div>
					</div>
				</a>
				<a href="mesCours">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">Mes Cours</div>
								
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>

		<!-- Deuxième bloc de boutons du menu -->
		<div class="row blocMenu">
			<div class="col-sm-12 fondBlanc">
				<?php if($_SESSION["RUE"]==true){ 
						if($_SESSION["UE_OK"] == $_SESSION["UE_TOTAL"]){
							$fond = "fondVert";
						} else {
							$fond = "fondRouge";
						}
						?>
				<a href="intervenants">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">
									Mes UEs <?php echo '<div class="nombreMenu '.$fond.'">'.$_SESSION["UE_OK"]."/".$_SESSION["UE_TOTAL"] ?></div>
								</div>
							</div>
						</div>
					</div>
				</a>
				<?php } ?>
				<?php if($_SESSION["RF"]==true){ ?>
				<a href="maFormation">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">Ma formation</div>
							</div>
						</div>
					</div>
				</a>
				<?php } ?>
				<?php if($_SESSION["RDI"]==true){ ?>
				<a href="pageDI">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">Mon Département</div>
							</div>
						</div>
					</div>
				</a>
				<?php } ?>
			</div>
		</div>

		<!-- Troisème bloc de boutons du menu -->
		<div class="row blocMenu">
			<div class="col-sm-12 fondBlanc">
				<a href="profil">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu">
									Mon Compte <div class="iconeMenu"><img src="images/iconeCompte.png" width="90%"></div>
								</div>
							</div>
						</div>
					</div>
				</a>
				<a href="deconnexion">
					<div class="row boutonMenu">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 contenuBoutonMenu texteRouge">
									Déconnexion <div class="iconeMenu"><img src="images/iconeDeconnexion.png" width="70%"></div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row bordureHaut">
			<div class="col-sm-9 col-sm-offset-3 col-xs-12">