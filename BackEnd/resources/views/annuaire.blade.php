<!-- FICHIER DE L'ANNUAIRE PERMETTANT LA RECHERCHE D'UN ENSEIGNANT -->

<!--  
Créé le : 13/05/2017
Modifié le : 17/05/2017
Responsable : Clément KOCH
-->

<?php require_once "includes/entete.php" ?> 

	<!-- TITRE -->
	<div class="row decalageHaut1">
		<div class="col-sm-10 titreVue">
			Annuaire
		</div>
	</div>

	<!-- BARRE DE RECHERCHE -->
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<form>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
						<input type="text" class="barreRecherche" id="rechercher" oninput="recherche()">
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- LIGNE GRISE -->
	<div class="row decalageHaut1">
		<div class="col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 bordureHaut texteGris paddingDroite0">
		Résultat de la recherche
		</div>
	</div>

	<!-- RESULTAT DE LA RECHERCHE -->
	<div class="row decalageHaut1">
		<div class="col-sm-5 col-sm-offset-1 col-xs-8 col-xs-offset-2">
			<div id="resultats">
				
			</div>
		</div>
		<div class="col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-2" id="fiche">
			
		</div>
	</div>
	

<?php require_once "includes/pied.php" ?>