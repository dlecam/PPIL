<!-- Page de connexion -->

<!--
Crée le : 13/05/2017
Modifié le : 25/05/2017
Responsable : Mohammade Dalati
-->

<?php
    if(isset($erreur)) {
        if($erreur != NULL){?>
            <script> window.alert(<?php echo $erreur; ?>);</script>
<?php }}?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>PPIL</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="styles/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="styles/styles.css" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <!-- TITRE -->
            <div class="row titreVue">
                <div class="col-sm-offset-4 col-sm-5 decalageBas1 alignementCentre">
                    Merci de vous identifier
                </div>
            </div>
            <!-- FORMULAIRE -->
            <form action="profil" method="POST" onsubmit="return verifForm();">
                <!-- IDENTIFIANT -->
                <div class="row decalBas">
                    <div class="col-sm-offset-5 col-sm-2">
                        <input class="form-control" type="text" placeholder="Identifiant" required="required" name="email" id="mail" onblur="verifMail(this)"/>
                    </div>
                </div>
                <!-- MOT DE PASSE -->
                <div class="row">
                    <div class="col-sm-offset-5 col-sm-2" style="padding-top: 1%">
                        <input class="form-control" type="password" placeholder="Mot de passe" required="required" name="password" id="pass"/>
                
                        <!-- RECUPERATION DU MOT DE PASSE -->
                        <div class="row texteOrange gras">
                            <div class="col-xs-offset-5 col-xs-7 alignementDroit">
                                <a href="resetPassword" style="padding-left: 9%">Mot de passe oublié ?</a>
                            </div>
                        </div>
                        <!-- BOUTON DE CONNEXION -->
                        <div class="row" style="padding-top: 2%">
                            <div class="col-xs-offset-3 col-xs-5 alignementCentre">
                                <input class="bouton" type="submit" value="Connexion"/> 
                            </div>
                        </div>
                        <!-- INSCRIPTION -->
                        <div class="row texteOrange gras">
                            <div class="col-xs-offset-3 col-xs-5 alignementCentre">
                                <a href="inscription">Inscription</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>         
        </div>
    </body>
</html>