<!-- Page d'informations personnelles -->

<!--  
Créé le : 15/05/2017
Modifié le : 20/05/2017
Responsable : Doriane LE CAM
-->

<?php require_once "includes/entete.php" ?> 

<?php
if(isset($_GET['erreur'])) {
if($_GET['erreur']==true){?>
<script> window.alert("Erreur lors de l'envoie du formulaire");</script>
<?php }}?>

    <!-- TITRE DE LA PAGE -->
    <div class="row decalageHaut1">
        <div class="col-sm-10 titreVue">
            Page personnelle
	</div>
    </div>
    
    <!-- LIEN VERS CHANGERMENT DE MDP -->
    <div class="row decalageHaut1">
        <div class="col-sm-offset-1 col-sm-3 col-xs-7">
            <a href="changePassword">
                <div class="gras texteOrange"> Modifier son mot de passe </div>
            </a>
        </div>
        
        <!-- LIEN VERS CHANGERMENT DE PHOTO DE PROFIL -->
        <div class="col-sm-offset-3 col-sm-3 col-xs-5">
            <a href="changePic">
                <div class="texteOrange gras"> Modifier sa photo </div>
            </a>
        </div>
    </div>
    
    <!-- UNIQUEMENT SUR LES TELEPHONE, PHOTO DE PROFIL EST AU DEBUT -->
    <div class="telephone row decalageHaut1">
        <div class="col-xs-12 decalageHaut1 decalageBas1">
            <div class="row">
                <div class="col-xs-offset-1 col-xs-10">
                    <div class=" fondGris photoProfil"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- DEBUT DU FORMULAIRE -->
    <div class="row decalageHaut2 decalageBas1">
        <div class="col-sm-6 bordureDroite">
            <form action="modifierProfil" method="post">
                <!-- NOM -->
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-offset-8 col-sm-6 col-xs-0 ordinateur texteFormulaire">      
                                Nom                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-7 col-xs-offset-2 col-xs-8">
                                <!-- récuppération du nom grâce à la fonction d'affichage -->
                                <input class="form-control" type="text" value=<?php if (isset($user)) { if ($user != NULL) { echo $user['nom'];}} ?> required="required" name="newNom"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PRENOM -->
                <div class="row decalageHaut1">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-offset-8 col-sm-6 col-xs-0 ordinateur texteFormulaire">                      
                                Prénom                   
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-7 col-xs-offset-2 col-xs-8">
                                <input class="form-control" type="text" value=<?php if (isset($user)) { if ($user != NULL) { echo $user['prenom'];}} ?> required="required" name="newPrenom"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- EMAIL -->
                <div class="row decalageHaut1">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-offset-8 col-sm-6 col-xs-0 ordinateur texteFormulaire">                      
                                Email                   
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-7 col-xs-offset-2 col-xs-8">
                                <input class="form-control" type="text" onblur="verifMail(this)" value=<?php if (isset($user)) { if ($user != NULL) { echo $user['email'];}} ?> required="required" name="newEmail"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- STATUT -->
                <div class="row decalageHaut1">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-offset-8 col-sm-6 col-xs-0 ordinateur texteFormulaire">                      
                                Statut                   
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-7 col-xs-offset-2 col-xs-8">
                                <input class="form-control" type="text" disabled="true" value=<?php if (isset($user)) { if ($user != NULL) { echo $user['statut'];}} ?> />
                            </div>
                        </div>
                    </div>
                </div>             
                
                <div class="row decalageHaut2">
                    <!-- BOUTON VALIDER -->
                    <div class="col-sm-offset-3 col-sm-3 col-xs-6">
                        <div class="row">
                            <div class="col-sm--offset-3 col-sm-6 col-xs-offset-1 col-xs-10">
                                <input class="bouton" type="submit" value="Valider"/>
                            </div>
                        </div>
                    </div>   
                    <!-- BOUTON ANNULER -->                    
                    <div class="col-sm-3 col-xs-6">
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10">
                                <a href="profil"><div class="bouton">Annuler</div></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="telephone decalageBas1"></div>
        <!-- UNIQUEMENT SUR ORDINATEUR, PHOTO SUR LA DROITE -->
        <div class="ordinateur col-sm-6">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8 alignementCentre">
                    <div class="fondGris photoProfil"></div>
                </div>
            </div>
        </div>
    </div>


<?php require_once "includes/pied.php" ?>