<!-- FICHIER DE L'ANNUAIRE PERMETTANT LA RECHERCHE D'UN ENSEIGNANT -->

<!--  
Créé le : 14/05/2017
Modifié le : 18/05/2017
Responsable : Valentin THOUVENIN
-->


<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>PPIL</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/styles.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    
    <script>
            function colorier(champ, erreur) {
                if (erreur){
                    champ.style.color = "red";                   
                } else {
                    champ.style.color = "";
                }
            }
            
            function verifMail(champ) {
                var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
                if(!regex.test(champ.value)) {
                    colorier(champ, true);
                    return false;
                } else {
                    colorier(champ, false);
                    return true;
                }
            }
            
            function verifNom(champ){
                var nom = champ.value;  
                var newNom = nom.trim();
                if(newNom.length < 3 || newNom.length > 25){
                    colorier(champ, true);
                    return false;
                } else {
                    colorier(champ, false);
                    return true;
                }
            }
            
            function verifStatut(champ){
                if(champ.value == 0){
                    return false;
                } else {
                    return true;
                }
            }
            
            function verifMdp(){
                var mdp = document.forms["inscription"].elements["password"].value;
                var confmdp = document.forms["inscription"].elements["confMdp"].value;
                if (mdp  != confmdp){
                    return false;
                } else {
                    return true;
                }
            }
            

            
            function verifForm(){
                var stat = verifStatut(document.forms["inscription"].elements["statut"]);
                var mdp = verifMdp();
                var mail = verifMail(document.forms["inscription"].elements["adresseMail"]);
                var nom = verifNom(document.forms["inscription"].elements["nom"]);
                var prenom = verifNom(document.forms["inscription"].elements["prenom"]);
                if (mdp && stat && mail && nom && prenom){  
                    return true; 
                } else {
                    alert("Veuillez remplir correctement les champs");
                    return false;
                }
            }          
                   
    </script>
        
  </head>
  <body>
    <div class="container-fluid">
        
        <!-- Retour connexion -->
        <div class="row texteOrange gras" style="margin-top: 2%; margin-left: 1%">
             <div class="col-xs-12 ">                
                 <a href="connexion">< Retour sur la page de connexion</a>
             </div>
        </div>
        
        <!-- TITRE -->
	<div class="row titreVue">
		<div class="col-lg-offset-5 col-lg-7 col-md-offset-4 col-md-8 col-sm-offset-4 col-sm-8">
			Inscription
		</div>
	</div>
        
        <!--Formulaire -->
        <form name="inscription" method="post" action="inscription" onsubmit="return verifForm();">
            <div class ="row">                       
                    <div class="col-md-offset-4 col-md-2 col-sm-offset-1 col-sm-5 col-xs-11" style="padding-bottom: 2%">
                        <input type="text" name="prenom" onblur="verifNom(this)" required="required" class="form-control" placeholder="Prénom">                   
                    </div>
                    <div class="col-md-2 col-sm-5 col-xs-11">
                        <input type="text" name="nom" onblur="verifNom(this)" required="required" class="form-control" placeholder="Nom">
                    </div>
            </div>
            <div class="visible-xs visible-sm decalageBas1">                
            </div>
            <div class ="row">     
                    <div class="col-md-offset-4 col-md-2 col-sm-offset-1 col-sm-5 col-xs-11" style="padding-bottom: 2%">
                        <input type="text" name="email" onblur="verifMail(this)" required="required" class="form-control" placeholder="Adresse email">
                    </div> 

                    <div class="col-md-2 col-sm-5 col-xs-11">
                        <select class="form-control" id="statut" name="statut" >
                            <option value="0" selected hidden>Statut</option>
                            <option value="1">Enseignant-chercheur</option>
                            <option value="2">Doctorant</option>
                            <option value="3">Professeur agrégé</option>
                            <option value="4">Vacataire</option>
                            <option value="5">ATER</option>
                            <option value="6">PRAG</option>
                        </select>
                    </div>                    
            </div>
            <div class="visible-xs visible-sm decalageBas1">                
            </div>
            <div class ="row">     
                    <div class="col-md-offset-4 col-md-2 col-sm-offset-1 col-sm-5 col-xs-11" style="padding-bottom: 2%">
                        <input type="password" name="password" class="form-control" required="required" placeholder="Mot de passe">
                    </div>  
                    <div class="col-md-2 col-sm-5 col-xs-11">
                        <input type="password" name="confMdp" class="form-control" required="required" placeholder="Confirmation mdp">
                    </div>  
            </div>
            <div class="visible-xs visible-sm decalageBas1">                
            </div>
            <div class ="row"> 
                <div class="col-md-offset-5 col-md-2 col-xs-offset-8 col-xs-4" style="padding-bottom: 2%; margin-left: 45%">
                    <input type="submit" class="bouton" id="validation" value=" INSCRIPTION ">
                </div>
            </div>
        </form>
        
    </div>
  </body>

