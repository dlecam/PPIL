<!-- PAGE D'INSCRIPTION À UNE UE -->

<!--  
Créé le : 16/05/2017
Modifié le : 20/05/2017
Responsable : Benjamin THIRION
-->

<?php require_once "includes/entete.php" ?> 

	<!-- TITRE -->
	<div class="row decalageHaut1">
		<div class="col-sm-10 titreVue">
			Inscription à une UE
		</div>
	</div>

	<!-- FORMULAIRE -->
        <form id="ajouterUeForm" method="POST" action="inscription_ue">
	
		<!-- SELECTEUR DE FORMATION + VALIDER -->
		<div class="row">
			<div class="col-sm-3 col-sm-offset-1">
				<select id="formation">
					<option value="all">Formation</option>
                                        <?php foreach ($formations as $form) { ?>
                                            <option value="li1"><?php echo $form['nomFormation']; ?></option>
                                     <?php   } ?>
                                </select>
			</div>
			
			<div class="col-sm-1"><input type="submit" class="bouton" value="Valider"></div>
		</div>
		
		<!-- TABLEAU DES UE -->
		<div class="row tableau decalageHaut1">
			<div class="col-sm-2 gras alignementCentre">Formation</div>
			<div class="col-sm-2 gras alignementCentre">UE</div>
			<div class="col-sm-1 gras alignementDroit">Valider</div>
		</div>
		
		<div id="li1Rows" class="ueRows">
                    <?php foreach ($formations as $formation) { ?>
                                           
			<div class="row tableau">
				<div class="col-sm-2 alignementCentre"><?php echo $formation['nomFormation'];?></div>
				<div class="col-sm-2 alignementCentre"><?php echo $formation['nomUE'];?></div>
				<div class="col-sm-1 alignementDroit"><input type="checkbox" name="valide" value="4"></div>
			</div>
                    <?php } ?>

		</div>	
	</form>

	
	
	<script type="text/javascript">
		var formSelect = document.getElementById('formation');
		
		formSelect.addEventListener('change', function() {
			var ue = formSelect[formSelect.selectedIndex].value;
			
			if(ue !== 'all') {
				[].forEach.call(document.querySelectorAll('.ueRows'), function (rows) {
					rows.style.display = 'none';
				});
				
				document.getElementById(ue + 'Rows').style.display = 'block';
			}
			else {
				[].forEach.call(document.querySelectorAll('.ueRows'), function (rows) {
					rows.style.display = 'block';
				});
			}
		});
	</script>
	

<?php require_once "includes/pied.php" ?>