<!-- FICHIER POUR QUE TOUT LES ENSEIGNANTS AIENT ACCES A LEURS HEURES -->

<!--  
Créé le : 15/05/2017
Modifié le : 24/05/2017
Responsable : Clément KOCH
-->

<?php require_once "includes/entete.php";

if(isset($results["total"])){


?> 

	<!-- TITRE -->
	<div class="row decalageHaut1">
		<div class="col-sm-6 titreVue">
			Mes cours
		</div>
	</div>
	<div class="row decalageBas1 decalageDroite1">
		<div class="col-sm-10 texteRouge titre4">
			<?php if(isset($_GET['erreur'])){echo $_GET['erreur'];} ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4 col-sm-offset-7 col-xs-6 col-xs-offset-5 alignementDroit">
			<span class="gras texteNoir titre4">Balance totale : &nbsp</span>
			<div class="boutonPetit fondBleu positionnementDroit">
				<?php echo $results["total"]["effetue"];echo "/";echo $results["total"]["total"]?>		
			</div>
		</div>
	</div>
	<div class="row decalageHaut1">
		<div class="col-sm-2 col-sm-offset-9 col-xs-6 col-xs-offset-5 decalageHautBas">
			<a href="inscription_ue"><div class="boutonPetit positionnementDroit texteBlanc" label="S'inscrire à un cour">
				+
			</div></a>
		</div>
	</div>

	<?php
}

	if(isset($results["cours"])){
		
		foreach($results["cours"] as $cour){

			$total = 1.5*$cour["CM"]+(2*$cour["TP"]/3)+$cour["TD"]+(7*$cour["EI"]/6);

			echo
			'<!-- T2 Optimisation -->
			<div class="row">
				<div class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10 bandeau fondGris">';

			echo $cour["nom"];

			echo
			'	</div>
			</div>
			<!-- Titre du tableau -->
			<div class="row decalageHaut1 tableau">
				<!-- Ordinateur -->
				<div class="col-sm-1 col-sm-offset-5 gras alignementCentre ordinateur">CM</div>
				<div class="col-sm-1 gras alignementCentre ordinateur">TD</div>
				<div class="col-sm-1 gras alignementCentre ordinateur">TP</div>
				<div class="col-sm-1 gras alignementCentre ordinateur">EI</div>
				<div class="col-sm-2 gras alignementCentre ordinateur">Total heures TD</div>

				<!-- Téléphones -->
				<div class="col-xs-4 col-xs-offset-3 gras telephone alignementCentre decalageHautBas">Total des heures</div>
			</div>

			<!-- Total des heures -->
			<div class="row tableau">
				<!-- Ordinateur -->
				<div class="col-sm-4 col-sm-offset-1 gras bordureBas ordinateur decalageHautBas">Total des heures</div>';

			echo '<div class="col-sm-1 bordureBas alignementCentre ordinateur decalageHautBas">'.$cour["CM"].'h</div>';
			echo '<div class="col-sm-1 bordureBas alignementCentre ordinateur decalageHautBas">'.$cour["TD"].'h</div>';
			echo '<div class="col-sm-1 bordureBas alignementCentre ordinateur decalageHautBas">'.$cour["TP"].'h</div>';
			echo '<div class="col-sm-1 bordureBas alignementCentre ordinateur decalageHautBas">'.$cour["EI"].'h</div>';
			echo '<div class="col-sm-2 bordureBas alignementCentre ordinateur decalageHautBas">'.$total.'h</div>';

			echo '<!-- Téléphones -->
				<div class="col-xs-2 col-xs-offset-1 gras telephone alignementCentre bordureBas">CM</div>
				<div class="col-xs-4 telephone alignementCentre bordureBas">'.$cour["CM"].'h</div>
			</div>';

			echo '
			<div class="row tableau">
				<!-- Téléphones -->
				<div class="col-xs-2 col-xs-offset-1 gras telephone alignementCentre bordureBas">TD</div>
				<div class="col-xs-4 telephone alignementCentre bordureBas">'.$cour["TD"].'h</div>
			</div>';

			echo '
			<!-- Téléphones -->
			<div class="row tableau">
				<div class="col-xs-2 col-xs-offset-1 gras telephone alignementCentre bordureBas">TP</div>
				<div class="col-xs-4 telephone alignementCentre bordureBas">'.$cour["TP"].'h</div>
			</div>';

			echo '
			<!-- Téléphones -->
			<div class="row tableau">
				<div class="col-xs-2 col-xs-offset-1 gras telephone alignementCentre bordureBas">EI</div>
				<div class="col-xs-4 telephone alignementCentre bordureBas">'.$cour["EI"].'h</div>
			</div>';

			echo '
			<!-- Téléphones -->
			<div class="row tableau decalageBas1">
				<div class="col-xs-2 col-xs-offset-1 gras telephone alignementCentre">Total</div>
				<div class="col-xs-4 telephone alignementCentre">'.$total.'h</div>
			</div>';


		}

	?>

	
		<!-- T2 Ajouter des heures -->
		<div class="row decalageHaut2">
			<div class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10 bandeau fondGris gras">
				Ajouter des heures
			</div>
		</div>

		<div class="row decalageHaut1 decalageBas1">
			<div class="col-sm-1 col-sm-offset-1 col-xs-12 alignementCentre decalageHautBas">
				<form method="POST" action="ajouterHeure">	
				<select name="id">
					<?php 
					foreach($results["cours"] as $cour){
						echo '<option value="'.$cour["id"].'">'.$cour["nom"].'</option>';
					}
					?>
				</select>
			</div>
			<div class="col-sm-2 col-sm-offset-1 col-xs-12 alignementCentre decalageHautBas">
				<input type="text" name="volume" size="6" placeholder="Volume">	
				<select name="type">
					<option value="CM">CM</option>
					<option value="TD">TD</option>
					<option value="TP">TP</option>
					<option value="EI">EI</option>
				</select>
			</div>
			<div class="col-sm-2 col-xs-12 alignementCentre decalageHautBas">
				<input type="submit" value="Ajouter" class="bouton"></form>
			</div>
		</div>

		<?php
	}

	if(isset($results["notif"])){

		foreach ($results["notif"] as $notif) {
			echo '
			<div class="row decalageHautBas">
				<div class="col-sm-offset-1 col-sm-9 col-xs-offset-1 col-xs-9 bordureBas">
					<span class="gras">'.$notif["date"].'</span><span class="texteBleu ordinateur">- En attente de validation -</span> Vous avez ajouté '.$notif["heure"].' heures de '.$notif["type"].' en '.$notif["nom"].'
				</div>
				<div class="col-sm-2 col-xs-1"><form method="POST" action="supprimerHeure"><input type="hidden" name="id" value="'.$notif["id"].'"><input type="submit" class="boutonPetit fondRouge" value="Annuler"></form></div>
			</div>';
		}
	}

	?>

	<div class="decalageBas1"></div>
