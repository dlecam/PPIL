<!-- Page de modification du mot de passe -->

<!--  
Créé le : 13/05/2017
Modifié le : 20/05/2017
Responsable : Doriane LE CAM
-->

<?php require_once "includes/entete.php" ?> 
      
<?php
if(isset($_GET['erreur'])) {
if($_GET['erreur']==true){?>
<script> window.alert("Erreur lors de l'envoie du formulaire");</script>
<?php }}?>

    <!-- TITRE DE LA PAGE -->
    <div class="row decalageHaut1">
        <div class="col-sm-10 titreVue">
            Modifier son mot de passe
	</div>
    </div>
    
    <!-- FORMULAIRE MODIFICATION -->
    <div class="row">
        <div class="col-sm-12">
            <form method="post" action="changePassword">
                <div class="row decalageHaut1">
                    <div class="col-sm-offset-1 col-sm-3 col-xs-10 texteFormulaire">
                        Ancien mot de passe
                    </div>
                    <div class="col-sm-3 col-xs-offset-1 col-xs-8">
                        <input class="form-control" type="password" name="oldPassword">
                    </div>
                </div>
                <div class="row decalageHaut1">
                    <div class="col-sm-offset-1 col-sm-3 col-xs-10 texteFormulaire">
                        Nouveau mot de passe
                    </div>
                    <div class="col-sm-3 col-xs-offset-1 col-xs-8">
                        <input class="form-control" type="password" name="newPassword">
                    </div>
                </div>    
                <div class="row decalageHaut1">
                    <div class="col-sm-offset-1 col-sm-3 col-xs-10 texteFormulaire">
                        Confirmer nouveau mot de passe
                    </div>
                    <div class="col-sm-3 col-xs-offset-1 col-xs-8">
                        <input class="form-control" type="password" name="newPasswordConfirm">
                    </div>
                </div>
                
                <div class="row decalageHaut2 decalageBas1">   
                    <!-- BOUTON VALIDER -->
                    <div class="col-sm-offset-3 col-sm-3 col-xs-offset-1 col-xs-5">
                        <input class="bouton" type="submit" value="Valider"/>
                    </div>
                    <!-- BOUTON ANNULER -->
                    <div class="col-sm-3 col-xs-5">                                          
                        <a href="profil"><div class="bouton">Annuler</div></a>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php require_once "includes/pied.php" ?>