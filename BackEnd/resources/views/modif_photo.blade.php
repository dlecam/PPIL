<!-- PAGE DE MODIFICATION DE LA PHOTO DE PROFIL -->

<!--  
Créé le : 15/05/2017
Modifié le : 20/05/2017
Responsable : Benjamin THIRION
-->

<?php require_once "includes/entete.php" ?> 

	<!-- TITRE -->
	<div class="row decalageHaut1">
		<div class="col-sm-10 titreVue">
			Photo de profil
		</div>
	</div>

	<!-- FORM + PHOTO -->
	<div class="row">
		<div class="col-xs-offset-1 col-md-10">
			
			<!-- FORMULAIRE -->
			<div class="col-md-5">
				<form action="changePic" method="POST" enctype="multipart/form-data">
					<div class="row">
						<input type="file" accept="image/*" name="photo" required>
					</div>
					
					<div class="row decalageHaut1 decalageBas1">
						<div class="col-xs-4"><input type="submit" class="bouton" value="Importer"></div>
						<div class="col-xs-4 col-xs-offset-2"><a href="profil"><div class="bouton">Retour</div></a></div>
					</div>
				</form>
			</div>
			
			<!-- PHOTO -->
			<div class="col-md-5 photoProfil fondGris"><img src="<?php if(isset($urlPhoto)) echo $urlPhoto; ?>"></div>
			
		</div>
	</div>

<?php require_once "includes/pied.php" ?>