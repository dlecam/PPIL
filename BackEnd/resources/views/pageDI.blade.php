<!-- Fichier permettant au responsable du département informatique de voir les heures réalisées par les enseignants -->

<!--
Crée le : 15/05/2017
Modifié le : 25/05/2017
Responsable : Mohammade Dalati
-->

<?php
    if(isset($erreur)) {
        if($erreur != NULL){?>
            <script> window.alert(<?php echo $erreur; ?>);</script>
<?php }}?>
            
<?php require_once "includes/entete.php" ?>

    <!-- TITRE -->
    <div class="row decalageHaut1">
        <div class="col-sm-10 titreVue">
            Liste des enseignants
        </div>
    </div>
    <!-- BOUTON IMPORT -->
    <div class="row">
        <form action="" method="POST">
            <div class="row">
                <div class="col-xs-offset-1 col-xs-8">
                    <input type="file" accept="image/*" name="photo" required/>
                </div>
            </div>
            <div class="row decalageHautBas">
                <div class="col-xs-offset-1 col-xs-2">
                    <input type="submit" class="bouton" value="Importer"/>
                </div>
            </div>
        </form>
    </div>
    
    <!-- TABLEAU -->
    <div class="row decalageHaut1 tableau">
        <div class="col-xs-2 col-md-4 gras alignementCentre">Nom</div>
        <div class="col-xs-offset-1 col-xs-2 col-md-offset-0 col-md-2 gras alignementCentre">Statut</div>
        <div class="col-xs-offset-1 col-xs-2 col-md-offset-0 col-md-2 gras alignementCentre">Heures à réaliser</div>
        <div class="col-xs-offset-1 col-xs-2 col-md-offset-0 col-md-2 gras alignementCentre">Heures réalisées</div>
    </div>
    
    <?php
        if (isset($users)) {
            if ($users != NULL) {
                foreach ($users as $user) { ?>
                    
    <div class="row decalageHautBas tableau">
        <div class="col-xs-1 col-md-4 alignementCentre texteOrange"><a href="mesCours"><?php echo $user['prenom'].' '.$user['nom']; ?></a></div>
        <div class="col-xs-offset-1 col-xs-2 col-md-offset-0 col-md-2 alignementCentre">
            <form action="changeStatut" method="POST">
                <select name="statut">
                    <option selected><?php echo $user['statut'];?></option>
                    <?php foreach ($user['statutRestant'] as $statutRestant) { ?>
                    <option><?php echo $statutRestant; ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="id" value=<?php echo $user['id']; ?> />
                <input type="submit" value="Valider"/>
            </form>
        </div>
        <div class="col-xs-offset-2 col-xs-2 col-md-offset-0 col-md-2 alignementCentre"><?php echo $user['heureAFaire']; ?></div>
        <div class="col-xs-offset-1 col-xs-2 col-md-offset-0 col-md-2 alignementCentre"><?php echo $user['heureFaite']; ?></div>
        <div class="col-xs-12 col-md-offset-1 col-md-9 bordureBas"></div>
    </div>
    
    <?php } } } ?>

<?php require_once "includes/pied.php" ?>
