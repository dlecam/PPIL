<!-- Page des UEs d'une formation -->

<!--  
Créé le : 15/05/2017
Modifié le : 21/05/2017
Responsable : Valentin THOUVENIN
-->

<?php require_once "includes/entete.php" ?>

    <script>                  
        function getFormation(forma){
            if (forma == 1){         
                document.getElementById('l3').style.display='none';
                document.getElementById('l1').style.display='block';
                document.getElementById('l1').style.visibility='visible';     
            }
            if (forma == 2){   
                document.getElementById('l1').style.display='none';
                document.getElementById('l3').style.display='block';
                document.getElementById('l3').style.visibility='visible';     
            }
        }
    </script>
    
    <?php foreach ($formations as $formation) { ?>
        <!-- TITRE -->
        <div class="row decalageHaut1">
            <div class="col-sm-9 titreVue paddingDroite0">
                UEs de la formation : <?php echo $formation['formation'].' '.$formation['annee']; ?>
        </div>

    </div>
    
    <div id ="l3">


        <!-- TITRE2 -->
        <?php foreach ($formation['ues'] as $ue) { ?>
        <div class="row">
            <div class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10 decalageHaut1 bandeau fondGris ">
                        <?php echo $ue['nom']; ?>
            </div>
        </div>
        <!-- TITRE TABLEAU -->
        <div class="row decalageHaut1 tableau">
            <div class="col-xs-offset-3 col-sm-2 col-xs-2 gras alignementCentre">Nombre de groupes attendus</div>
            <div class="col-sm-2 col-xs-2 gras alignementCentre">Volume affecté</div>        
        </div>

        <!-- LIGNE CM -->
        <div class="row decalageHautBas tableau">
            <div class="col-xs-offset-1 col-sm-2 col-xs-2 gras bordureBas alignementCentre">CM</div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre"></br></div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre texteVert"><?php echo $ue['CM']; ?></div>
        </div>

        <!-- LIGNE TD -->
        <div class="row decalageHautBas tableau">
            <div class="col-xs-offset-1 col-sm-2 col-xs-2 gras bordureBas alignementCentre">TD</div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre"><?php echo $ue['groupeTD']; ?></div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre texteVert"><?php echo $ue['TD']; ?></div>
        </div>

        <!-- LIGNE TP -->
        <div class="row decalageHautBas tableau">
            <div class="col-xs-offset-1 col-sm-2 col-xs-2 gras bordureBas alignementCentre">TP</div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre"><?php echo $ue['groupeTP']; ?></div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre texteVert"><?php echo $ue['TP']; ?></div>
        </div>

        <!-- LIGNE EI -->
        <div class="row decalageHautBas tableau">
            <div class="col-xs-offset-1 col-sm-2 col-xs-2 gras bordureBas alignementCentre">EI</div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre"><?php echo $ue['groupeEI']; ?></div>
            <div class="col-sm-2 col-xs-2 bordureBas alignementCentre"><?php echo $ue['EI']; ?></div>
        </div>

        <!-- LIGNE TOTAL -->
        <div class="row decalageHautBas tableau">
            <div class="col-xs-offset-1 col-sm-2 col-xs-2 gras alignementCentre">Total</div>
            <div class="col-sm-2 col-xs-2 alignementCentre"></br></div>
            <div class="col-sm-2 col-xs-2 alignementCentre texteVert"><?php echo $ue['Total']; ?></div>
        </div>   

    <?php }} ?>



 

    </div>
    
    <div class="row decalageBas1"></div>
<?php require_once "includes/pied.php" ?>

