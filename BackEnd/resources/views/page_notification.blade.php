<!-- Page du journal de modification (notification -->

<!--  
Créé le : 14/05/2017
Modifié le : 20/05/2017
Responsable : Doriane LE CAM
-->

<?php require_once "includes/entete.php"?> 

    <!-- TITRE DE LA PAGE -->
    <div class="row decalageHaut1">
        <div class="col-sm-10 titreVue">
            Notifications
	</div>
    </div>
    <!-- BANDEAU GRIS -->
    <div class="row decalageBas1">
        <div class="col-sm-offset-1 col-sm-10 bandeau fondGris gras"> En attente de validation </div>
    </div>
    
    <!-- NOTIF A VALIDER -->
       
    <?php foreach($notifications as $notification) { ?>

    <div class="row">
        <div class="col-sm-offset-1 col-sm-6">
            <div class="row">
                <div class="col-sm-12 col-xs-12 texteNotif">
                    <?php echo $notification["message"]; ?>
                </div>
            </div>
        </div>
         <!-- BOUTONS REFUSER -->
        <div class="col-sm-offset-1 col-sm-4">
            <div class="row">
                <div class="col-sm-5 col-xs-6">
                    <form action="refuserNotification" method="post">
                        <input type="hidden" name="id" value=<?php echo $notification["id"];?> />
                        <input type="hidden" name="type" value=<?php echo $notification["type"];?> />
                        <input class="bouton fondRouge" type="submit" value="Refuser" />
                    </form>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <form action="validerNotification" method="post">
                        <input type="hidden" name="id" value=<?php echo $notification["id"];?> />
                        <input type="hidden" name="type" value=<?php echo $notification["type"];?> />
                        <input class="bouton fondVert" type="submit" value="Valider" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- LIGNE GRISE UNIQUEMENT POUR L'ORDINATEUR -->
    <div class="row ordinateur">
        <div class=" col-sm-offset-1 col-sm-10 bordureBas paddingDroite0 decalageHautBas"></div>
    </div>
    <?php } ?>
    
<?php require_once "includes/pied.php" ?>