<!-- FICHIER DE VISUALISATION DES QUOTAS D'UNE UE -->

<!--  
Cree le : 15/05/2017
Modifie le : 25/05/2017
Responsable : Laura WEBER
-->

<?php require_once "includes/entete.php" ?> 

    <?php
    if(isset($_GET['erreur'])) {
    if($_GET['erreur']==true){?>
    <script> window.alert("Erreur lors de l'envoie du formulaire");</script>
    <?php }}?>

    <script>            
        
        function getQuotasUE(UE){
            var taille = <?php echo count($ues); ?>;
            var i;
            if(UE != 0){
                for(i = 1; i < taille + 1; i++){ 
                    document.getElementById(i).style.display = "none";
                }
                document.getElementById(UE).style.display = "block";
                document.getElementById(42).style.display = "block";
                document.getElementById(43).style.display = "block";
            }else{
                document.getElementById(42).style.display = "none";
                document.getElementById(43).style.display = "none";
                for(i = 1; i < taille + 1; i++){ 
                    document.getElementById(i).style.display = "none";
                }
            }
        }
        
    </script>

    <!-- TITRE -->
	<div class="row decalageHaut1">
		<div class="col-sm-10 titreVue">
                    Résumé des quotas
                </div>
	</div>
        
    <!-- CHAMP DE SAISIE DE L'UE -->
        <div class="row">
            <div class="col-sm-9 col-xs-11 texteFormulaire">
                <form action='ajouterIntervenant' method='post'>
                    <div class="row decalageHaut1 decalageBas1">
                        <div class="col-sm-offset-2 col-sm-3">
                             <select name="ue" onchange="getQuotasUE(this.selectedIndex)">
                                <option value="defaut" selected> Selectionnez une UE...</option>
                                <?php
                                foreach ($ues as $ue){
                                    echo '<option value="'.$ue['id'].'">'.$ue['nom'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row decalageHaut1 decalageBas1">
                        <div id='42' style='display:none;' class='col-sm-offset-2 col-xs-3'>
                            <select name='intervenant'>
                                <option value='defaut' selected> Selectionnez un intervenant...</option>
                                <?php
                                foreach ($ue['nonProfs'] as $prof){
                                    echo '<option value="'.$prof['id'].'">'.$prof['prenom'].' '.$prof['nom'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div id='43' style='display:none;' class='col-xs-offset-1 col-xs-3'>
                            <input class='bouton' type='submit' value='Ajouter'>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
    <!-- VISUALISATION DES QUOTAS -->
    
    <?php
        $tmp = 0;
        foreach ($ues as $ue){
            $tmp++;
            echo "<div id =".$tmp." name='quotas' style='display:none;'>";
                echo "<div class='row decalageHaut1'>";
                echo "<div class='col-sm-offset-1 col-xs-10 bandeau fondGris'>".$ue['nom']."</div>";
                echo "</div>";
                echo "<!-- Titre du tableau -->";
                echo "<div class='row gras tableau decalageHaut1'>";
                echo "<!-- Ordinateur -->";
                echo "<div class='col-sm-offset-6 col-sm-1 alignementCentre ordinateur'>CM</div>";
                echo "<div class='col-sm-1 alignementCentre ordinateur'>TD</div>";
                echo "<div class='col-sm-1 alignementCentre ordinateur'>TP</div>";
                echo "<div class='col-sm-1 alignementCentre ordinateur'>EI</div>";
                echo "<div class='col-sm-1 alignementCentre ordinateur'>Supprimer</div>";
                echo "<!-- Telephone -->";
                echo "<div class='col-xs-offset-2 col-xs-2 alignementCentre telephone'>CM</div>";
                echo "<div class='col-xs-2 alignementCentre telephone'>TD</div>";
                echo "<div class='col-xs-2 alignementCentre telephone'>TP</div>";
                echo "<div class='col-xs-2 alignementCentre telephone'>EI</div>";
                echo "<div class='col-xs-1 alignementCentre telephone'></div>";
                echo "</div>";
                echo "<!-- Données du tableau -->";
                echo "<!-- Ordinateur -->";
                foreach ($ue['profs'] as $value){
                    echo "<div class='row decalageHautBas tableau'>";
                    echo "<div class ='col-sm-offset-1 col-sm-5 gras bordureHaut ordinateur'>".$value['prenom']." ".$value['nom']."</div>";
                    echo "<div class='col-sm-1 bordureHaut alignementCentre ordinateur'>".$value['CM']."h</div>";
                    echo "<div class='col-sm-1 bordureHaut alignementCentre ordinateur'>".$value['TP']."h</div>";
                    echo "<div class='col-sm-1 bordureHaut alignementCentre ordinateur'>".$value['TD']."h</div>";
                    echo "<div class='col-sm-1 bordureHaut alignementCentre ordinateur'>".$value['EI']."h</div>";
                    echo "<div class='col-sm-1 bordureHaut alignementCentre ordinateur'>";
                    echo "<form action='supprimerIntervenant' method='post'>";
                    echo "<input name='intervenant' style='display:none;' value='".$value['id']."'/>";
                    echo "<input name='ue' style='display:none;' value='".$ue['id']."'/>";
                    echo "<input class='boutonSupprimer' type='submit' value='x'/>";
                    echo "</form>";
                    echo "</div>";
                    echo "</div>";    
                }
                echo "<div class='row decalageHautBas tableau'>";
                echo "<div class='col-sm-offset-1 col-sm-3 gras bordureHaut ordinateur'>Heures totales effectuées : </div>";
                echo "<div class='col-sm-2 bordureHaut alignementCentre ordinateur'>".$ue['fait']."h</div>";
                echo "<div class='col-sm-3 bordureHaut alignementCentre gras ordinateur'>Heures totales à faire :</div>";
                echo "<div class='col-sm-2 bordureHaut alignementCentre ordinateur'>".$ue['volume']."h</div>";
                echo "</div>";
                echo "<!-- Telephone -->";
                foreach ($ue['profs'] as $value){
                    echo "<div class='row decalageHautBas tableau'>";
                    echo "<div class='col-xs-2 bordureBas gras telephone'>".$value['nom']."</div>";
                    echo "<div class='col-xs-2 alignementCentre bordureBas telephone'>".$value['CM']."h</div>";
                    echo "<div class='col-xs-2 alignementCentre bordureBas telephone'>".$value['TP']."h</div>";
                    echo "<div class='col-xs-2 alignementCentre bordureBas telephone'>".$value['TD']."h</div>";
                    echo "<div class='col-xs-2 alignementCentre bordureBas telephone'>".$value['EI']."h</div>";
                    echo "<div class='col-sm-1 alignementCentre telephone'>";
                    echo "<form action='supprimerIntervenant' method='post'>";
                    echo "<input name='intervenant' style='display:none;' value='".$value['id']."'/>";
                    echo "<input name='ue' style='display:none;' value='".$ue['id']."'/>";
                    echo "<input class='boutonSupprimer' type='submit' value='x'/>";
                    echo "</form>";
                    echo "</div>";
                    echo "</div>";
                }
                echo "<div class='row decalageHautBas tableau'>";
                echo "<div class='col-xs-4 bordureBas gras telephone'>Heures effectuées : </div>";
                echo "<div class='col-xs-1 alignementCentre bordureBas telephone'>".$ue['fait']."h</div>";
                echo "<div class='col-xs-4 alignementCentre bordureBas gras telephone'>Heures à faire : </div>";
                echo "<div class='col-xs-1 alignementCentre bordureBas telephone'>".$ue['volume']."h</div>";
                echo "</div>";
            echo "</div>";
        }
    ?>

<?php require_once "includes/pied.php" ?>