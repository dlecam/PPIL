<!-- FICHIER DE LA REINITIALISATION DU MDP -->

<!--  
Cree le : 15/05/2017
Modifie le : 18/05/2017
Responsable : Laura WEBER
-->

<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>PPIL</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/styles.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	
	<?php
	if(isset($_GET['erreur'])) {
	if($_GET['erreur']==true){?>
	<script> window.alert("Erreur lors de l'envoie du formulaire");</script>
	<?php }}?>
	
    <script>
        
        var mail;
        
        function colorier(champ, erreur) {
            if (erreur)
                champ.style.color = "red";
            else
                champ.style.color = "";
        }
        
        function verifMail(champ) {
            var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
            if(!regex.test(champ.value)) {
                colorier(champ, true);
                return false;
            } else {
                colorier(champ, false);
                return true;
                }
            }
            
        function setMail(champ){
            verifMail(champ);
            mail = champ;
        }
        
        function envoieMail(){
            var string = "\
                            <div class='row'>\n\
                                Un mail a bien était envoyé à l'adresse donnée.\n\
                            </div>\n\
                        ";
            if(verifMail(mail)){
                document.getElementById("message").innerHTML = string;
            }else{
                document.getElementById("message").innerHTML = "";
            }
        }
        </script>
  </head>
  <body>

    <!-- RETOUR A LA PAGE DE CONNEXION -->
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10 texteOrange decalageHaut1" >
                <a href="connexion">
                   &lt; retour à la page de connexion
                </a>
            </div>
        </div>
    
    <!-- TITRE -->
	<div class="row titreVue">
            <div class="col-sm-offset-1 col-lg-offset-3 col-xs-10">
		Réinitialiser le mot de passe
            </div>
	</div>
    
    <!-- MESSAGE D'ENVOIE D'UN MAIL (CONDITIONNEL) -->
        <div class="row">
            <div class="col-xs-offset-1 col-sm-offset-3 col-lg-offset-4 col-xs-10 texteRouge">
                <p id="message"></p
            </div>
        </div>
    
    <!-- CHAMP DE SAISIE DE L'ADRESSE MAIL -->
        <div class="row decalageHaut1">
            <div class="col-xs-offset-1 col-sm-offset-3 col-xs-10">
                <form name="reinit" method="post">
                    <div class="row col-lg-offset-1 alignementCentre texteFormulaire">
                        <div class="col-lg-2 col-sm-3 col-xs-3"> Adresse mail : </div>
                        <div class="col-xs-1">
                            <input name="email" type="text" onchange="setMail(this)"/>
                        </div>
                    </div>
                    <div class="row decalageHaut1 col-xs-offset-2">
                        <div class="col-xs-4 col-sm-3 col-lg-2 ">
                            <input class="bouton" type="submit" value="Envoyer" onclick="envoieMail()"/>
                        </div>
                        <div class="col-xs-4 col-sm-3 col-lg-2">
                            <a href="connexion">
                                <input class="bouton" type="button" value="Annuler"/>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

<?php require_once "includes/pied.php" ?>

