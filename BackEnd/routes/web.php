<?php

/*

routes.php
créé le samedi 13 mai
modifié le mercredi 17 mai
responsables : Steve, Thomas

*/

// à garder de côté 
// { echo json_encode($JSON, JSON_UNESCAPED_UNICODE); }

use Illuminate\Http\Request;


$app->get('/', function () use ($app) {
    return redirect()->route('profil');
});



// Connexion et deconnexion 

$app->get('/connexion', [ 'as' => 'connexion', function () {
	return view('connexion');
}]);

$app->get('/inscription', [ 'as' => 'inscription', function () {
	return view('inscription');
}]);

$app->post('/inscription', 'InscriptionController@inscrire');

$app->get('/deconnexion', 'ConnexionController@deconnexion');

// Reset password

$app->get('/resetPassword', [ 'as' => 'resetPassword', 'uses' => 'ProfilController@showResetPasswordPage']);

$app->post('/resetPassword', 'ProfilController@resetPassword');


// Routes authentifiées 

$app->group(['middleware' => 'auth'], function() use ($app) {
	
	// Page de profil
	
	$app->get('/profil', [ 'as' => 'profil', 'uses' => 'ProfilController@showProfilPage']);
	
	$app->post('/profil', 'ProfilController@showProfilPage');
	
	$app->post('/modifierProfil', 'ProfilController@modify');
	
	// Notifications
	
	$app->get('/notifications', ['as' => 'notifications', 'uses' => 'NotificationController@showNotificationPage']);
	
	$app->post('/validerNotification', 'NotificationController@validerNotification');
	
	$app->post('/refuserNotification', 'NotificationController@refuserNotification');
	
	// Page intervenant
	
	$app->get('/intervenants', ['as' => 'listeIntervenants', 'uses' =>'IntervenantController@afficherIntervenants']);
	
	$app->post('/ajouterIntervenant', 'IntervenantController@ajouterIntervenant');
	
	$app->post('/supprimerIntervenant', 'IntervenantController@supprimerIntervenant');
	
	// Changement de mot de passe
	
	$app->post('/changePassword', 'ProfilController@changePassword');

	$app->get('/changePassword', [ 'as' => 'changePassword', 'uses' => 'ProfilController@showPasswordPage']);
	
	// Recherche annuaire 
	
	$app->get('/annuaire', ['as' => 'annuaire', 'uses' => 'ResearchController@showPage']);
	
	$app->post('/annuaire', 'ResearchController@searchPerson');
	
	// Import

	$app->get('/import', 'ImportController@import');

    $app->post('/import', 'ImportController@import');

    // Export

    $app->get('/export', ['as' => 'export', 'uses' => 'ExportController@export']);
        
    // Mes cours
        
    $app->get('/mesCours',['as' => 'mesCours', 'uses' => 'MesCoursController@affiche'] );
        
	$app->post('/ajouterHeure', 'MesCoursController@ajouterHeure');
        
	$app->post('/supprimerHeure', 'MesCoursController@supprimerHeure');
        
    // Modifier photo profil
        
	$app->get('/changePic', ['as' => 'changePic', 'uses' => 'PhotoController@getForm']);
 
	$app->post('/changePic', 'PhotoController@postForm');

    // S'inscrire a une UE 
        
    $app->get('/intervention', ['as' =>'intervention', 'uses' => 'DemandeInscriptionUE@affiche']);
	
	$app->get('/inscription_ue', [ 'as' => 'inscription_ue','uses'=>'InscriptionUEController@listeInscriptionsUE']);

	$app->post('/inscription_ue', 'DemandeInterventionController@incriptionUE');
        
    // Consulter / modifier le status d'un enseignant par le RDI .
        
    $app->get('/pageDI', ['as' =>'pageDI', 'uses' => 'RespDIController@showDIPage']);
         
    $app->post('/changeStatut', 'RespDIController@modifStatut');
        
    // Ma formation
        
	$app->get('/maFormation', ['as' =>'maFormation', 'uses' => 'ConsultModifUEformationControllers@showPage']);
            
});



