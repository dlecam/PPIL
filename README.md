Deux répertoires sont crées. Un pour le groupe back et un pour le groupe front

Je rappelle le framapad du groupe
			-> https://bimestriel.framapad.org/p/Programmation

Pour le groupe front : 
	Seul Clément modifie et commit sur le fichier de style css. Les différents ajouts que vous voudriez y faire sont à écrire sur le framapad.
			-> https://mensuel.framapad.org/p/Front

	Cours/Rappels bootstrap :
			-> https://openclassrooms.com/courses/prenez-en-main-bootstrap/mise-en-route-8


Pour le groupe back :
			-> https://mensuel.framapad.org/p/PPILBackEnd
